![Edddy](banner.svg)

[![pipeline status](https://gitlab.com/luckeyproductions/tools/edddy/badges/master/pipeline.svg)](https://gitlab.com/luckeyproductions/tools/edddy/-/commits/master) [![coverage report](https://gitlab.com/luckeyproductions/tools/edddy/badges/master/coverage.svg)](https://gitlab.com/luckeyproductions/tools/edddy/-/commits/master)

**Edddy** is a block based 3D map editor built on [Dry](https://gitlab.com/luckeyproductions/dry). Its initial purpose is to create levels for [Blip 'n Blup](https://gitlab.com/luckeyproductions/games/blipnblup), [A-Mazing Urho](https://gitlab.com/luckeyproductions/games/amazingurho), [Tux!](https://gitlab.com/luckeyproductions/games/tux), [OG Tatt](https://gitlab.com/luckeyproductions/games/ogtatt), [KO](https://gitlab.com/luckeyproductions/games/ko) and [Octalloc](https://gitlab.com/luckeyproductions/games/octalloc). However, eventually Edddy should be suitable for any engine and approach [Tiled](https://mapeditor.org/) in terms of versatility; with things like terrain brushes, block clusters and map generation.

[DryBlend](https://gitlab.com/luckeyproductions/tools/DryBlend) and [TiNA](https://gitlab.com/luckeyproductions/tools/TiNA) both assist in creating suitable assets.

![Screenshot](Screenshots/Screenshot_2023-06-10_15-34-05.png)
![Screenshot](Screenshots/Screenshot_2023-06-14_13-43-52.png)
