#!/bin/sh

sudo chown -R $USER ~/.local/share/luckey/edddy/
sudo chown $USER ~/.local/share/icons/edddy.svg
update-icon-caches ~/.local/share/icons/
