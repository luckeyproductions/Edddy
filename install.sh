#!/bin/sh

./.installreq.sh
./.builddry.sh

git pull
qmake Edddy.pro -o ../Edddy-build/; cd ../Edddy-build
sudo make install; cd -; ./.postinstall.sh
rm -rf ../Edddy-build
