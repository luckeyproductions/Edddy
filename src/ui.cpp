/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDesktopWidget>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QStatusBar>
#include <QAction>

#include "view3d.h"
#include "docks/projectpanel.h"
#include "docks/mappanel.h"
#include "docks/blockspanel.h"

#include "mainwindow.h"

void MainWindow::setupUi()
{
    setObjectName("MainWindow");
    setWindowTitle(QCoreApplication::translate("MainWindow", "Edddy", nullptr));
    resize(1000, 693);
    setWindowIcon(QIcon{ ":/Edddy" });

    actionQuit_ = new QAction{ QIcon{ ":/Quit" }, "Quit", this };
    actionQuit_->setObjectName("actionQuit");
#if QT_CONFIG(shortcut)
    actionQuit_->setShortcut(QKeySequence{ "Ctrl+Q" });
#endif // QT_CONFIG(shortcut)

    actionNewProject_ = new QAction{ QIcon{ ":/New" }, "New Project", this };
    actionNewProject_->setObjectName("actionNewProject");
    actionOpenProject_ = new QAction{ QIcon{ ":/Open" }, "Open Project", this };
    actionOpenProject_->setObjectName("actionOpenProject");
    actionSaveProject_ = new QAction{ QIcon{ ":/SaveProject" }, "Save Project", this };
    actionSaveProject_->setObjectName("actionSaveProject");
#if QT_CONFIG(shortcut)
    actionOpenProject_->setShortcut(QKeySequence{ "Ctrl+O" });
    actionSaveProject_->setShortcut(QKeySequence{ "Ctrl+Shift+S" });
#endif // QT_CONFIG(shortcut)

    actionNewMap_ = new QAction{ QIcon{ ":/New" }, "New Map...", this };
    actionNewMap_->setObjectName("actionNewMap");
    actionSaveMap_ = new QAction{ QIcon{ ":/Save" }, "Save Map", this };
    actionSaveMap_->setObjectName("actionSaveMap");
#if QT_CONFIG(shortcut)
    actionNewMap_->setShortcut(QKeySequence{ "Ctrl+N" });
    actionSaveMap_->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)

    actionUndo_ = new QAction{QIcon{ ":/Undo" }, "Undo", this };
    actionUndo_->setObjectName("actionUndo");
#if QT_CONFIG(shortcut)
    actionUndo_->setShortcut(QKeySequence{ "Ctrl+Z" });
#endif // QT_CONFIG(shortcut)
    actionRedo_ = new QAction{ QIcon{ ":/Redo" }, "Redo", this };
    actionRedo_->setObjectName("actionRedo");
#if QT_CONFIG(shortcut)
    actionRedo_->setShortcuts({ QKeySequence{ "Ctrl+Shift+Z" }, QKeySequence{ "Ctrl+Y" } });
#endif // QT_CONFIG(shortcut)

    actionBrush_ = new QAction{ QIcon{ ":/Brush" }, "Brush", this };
    actionBrush_->setObjectName("actionBrush");
    actionBrush_->setCheckable(true);
#if QT_CONFIG(shortcut)
    actionBrush_->setShortcut(QKeySequence{ "B" });
#endif // QT_CONFIG(shortcut)
    actionFill_ = new QAction{ QIcon{ ":/Fill" }, "Fill", this };
    actionFill_->setObjectName("actionFill");
    actionFill_->setCheckable(true);
#if QT_CONFIG(shortcut)
    actionFill_->setShortcut(QKeySequence{ "F" });
#endif // QT_CONFIG(shortcut)
    actionDig_ = new QAction{ QIcon{ ":/Dig" }, "Dig", this };
    actionDig_->setObjectName("actionDig");
    actionDig_->setCheckable(true);
#if QT_CONFIG(shortcut)
    actionDig_->setShortcut(QKeySequence{ "V" });
#endif // QT_CONFIG(shortcut)

    connect(actionBrush_, SIGNAL(triggered(bool)), this, SLOT(pickBrush()));
    connect(actionFill_,  SIGNAL(triggered(bool)), this, SLOT(pickFill()));
    connect(actionDig_,   SIGNAL(triggered(bool)), this, SLOT(pickDig()));

    createAxisLockActions();

    actionSplitView_ = new QAction{ QIcon{ ":/SplitView" }, "Split View", this };
    actionSplitView_->setObjectName("actionSplitView");
    actionSplitView_->setEnabled(true);
#if QT_CONFIG(shortcut)
    actionSplitView_->setShortcut(QKeySequence{ "Alt++" });
#endif // QT_CONFIG(shortcut)

    actionFly_ = new QAction{ this };
    actionFly_->setToolTip("Fly mode");
    actionFly_->setIcon(QIcon{ ":/Fly"});
#if QT_CONFIG(shortcut)
    actionFly_->setShortcut(QKeySequence{ "Alt+F" });
#endif
    actionFly_->setCheckable(true);

    actionPlay_ = new QAction{ this };
    actionPlay_->setToolTip("Play");
    actionPlay_->setIcon(QIcon{ ":/Play" });
#if QT_CONFIG(shortcut)
    actionPlay_->setShortcut(QKeySequence("Alt+P"));
#endif
    actionPlay_->setCheckable(true);
    actionPlay_->setEnabled(false);

    const QSize cornerSize{ (QApplication::desktop()->geometry().size() - geometry().size()) / 2 };
    setGeometry(QRect{ QPoint{ cornerSize.width(), cornerSize.height() }, geometry().size() });

    createMenuBar();
    createToolBar();
    createStatusBar();
    createCentralStack();
    createPanels();
    connectSignals();

    retranslateUi();
}

void MainWindow::createAxisLockActions()
{
    actionLockX_ = new QAction{ QIcon{ ":/X" }, "Lock X", this };
    actionLockY_ = new QAction{ QIcon{ ":/Y" }, "Lock Y", this };
    actionLockZ_ = new QAction{ QIcon{ ":/Z" }, "Lock Z", this };

#if QT_CONFIG(shortcut)

    //Add shortcuts for inverse axis-locking
    for (QAction* a: { actionLockX_,
                       actionLockY_,
                       actionLockZ_ })
    {
        const QString axis{ a->text().right(1) };
        a->setObjectName("actionLock" + axis);
        a->setCheckable(true);
        a->setAutoRepeat(false);
        a->setShortcuts({ QKeySequence{ axis }, QKeySequence{ "Shift+" + axis } });
        connect(a, SIGNAL(triggered(bool)), this, SLOT(actionLockAxisTriggered()));
    }
#endif // QT_CONFIG(shortcut)
}

void MainWindow::createMenuBar()
{
    menuFile_ = new QMenu{ menuBar_ };
    menuFile_->setObjectName("menuTest");
    menuFile_->addAction(actionNewProject_);
    menuFile_->addAction(actionOpenProject_);
    menuFile_->addAction(actionSaveProject_);
    menuFile_->addSeparator();
    menuFile_->addAction(actionNewMap_);
    menuFile_->addAction(actionSaveMap_);
    menuFile_->addSeparator();
    menuFile_->addAction(actionQuit_);

    menuEdit_ = new QMenu{ menuBar_ };
    menuEdit_->setObjectName("menuEdit");
    menuEdit_->addAction(actionUndo_);
    menuEdit_->addAction(actionRedo_);
    menuMap_ = menuEdit_->addMenu("Map");
    menuMap_->setEnabled(false);

    menuTools_ = new QMenu{ menuBar_ };
    menuTools_->setObjectName("menuTools");
    menuTools_->addAction(actionBrush_);
    menuTools_->addAction(actionFill_);
    menuTools_->addAction(actionDig_);

    menuHelp_ = new QMenu{ menuBar_ };
    menuHelp_->setObjectName("menuHelp");
    actionAboutEdddy_ = new QAction{ QIcon{ ":/About" }, "About Edddy", this };
    actionAboutEdddy_->setObjectName("actionAboutEdddy");
    menuHelp_->addAction(actionAboutEdddy_);

    menuBar_ = new QMenuBar{ this };
    menuBar_->setObjectName("menuBar");
    menuBar_->addAction(menuFile_->menuAction());
    menuBar_->addAction(menuEdit_->menuAction());
    menuBar_->addAction(menuTools_->menuAction());
    menuBar_->addAction(menuHelp_->menuAction());
    setMenuBar(menuBar_);
}

void MainWindow::createToolBar()
{
    mainToolBar_ = new QToolBar{ "Main Toolbar", this };
    mainToolBar_->setObjectName("mainToolBar");
    mainToolBar_->addAction(actionSaveProject_);
    mainToolBar_->addSeparator();
    mainToolBar_->addAction(actionNewMap_);
    mainToolBar_->addAction(actionSaveMap_);
    mainToolBar_->addSeparator();
    mainToolBar_->addAction(actionUndo_);
    mainToolBar_->addAction(actionRedo_);
    mainToolBar_->addSeparator();
    mainToolBar_->addAction(actionBrush_);
    mainToolBar_->addAction(actionFill_);
    mainToolBar_->addAction(actionDig_);
    mainToolBar_->addSeparator();
    mainToolBar_->addAction(actionSplitView_);
    mainToolBar_->addAction(actionLockX_);
    mainToolBar_->addAction(actionLockY_);
    mainToolBar_->addAction(actionLockZ_);
    mainToolBar_->insertAction(actionSplitView_, actionFly_);
    mainToolBar_->insertAction(actionFly_, actionPlay_);

    createLayerButtons();

    addToolBar(Qt::TopToolBarArea, mainToolBar_);
}

void MainWindow::createStatusBar()
{
    statusBar_ = new QStatusBar{ this };
    statusBar_->setObjectName("statusBar");
    setStatusBar(statusBar_);
}

void MainWindow::createCentralStack()
{
    centralStackWidget_ = new QStackedWidget{ this };
    centralStackWidget_->setMinimumSize({ 320, 240 });

    noProjectWidget_ = new NoProjectWidget{ context_, this };

    centralStackWidget_->addWidget(noProjectWidget_);
    centralStackWidget_->addWidget(new View3D{ context_ });

    setCentralWidget(centralStackWidget_);
}

void MainWindow::createPanels()
{
    createDockWidget<ProjectPanel>(Qt::LeftDockWidgetArea);
    createDockWidget<MapPanel>(Qt::LeftDockWidgetArea);
    createDockWidget<BlocksPanel>(Qt::RightDockWidgetArea);

    undoView_ = new QUndoView{ this };
    undoView_->setEmptyLabel("Initial");
    QDockWidget* undoDockWidget{ new QDockWidget{ "Undo History", this } };
    undoDockWidget->setObjectName("Undo History");
    undoDockWidget->setWidget(undoView_);
    addDockWidget(Qt::RightDockWidgetArea, undoDockWidget);
}

void MainWindow::connectSignals()
{
    connect(noProjectWidget_, SIGNAL(newProject()), actionNewProject_,  SLOT(trigger()));
    connect(actionNewProject_, SIGNAL(triggered(bool)), this, SLOT(newProject()));
    connect(noProjectWidget_, SIGNAL(openProject()), actionOpenProject_, SLOT(trigger()));
    connect(actionOpenProject_, SIGNAL(triggered(bool)), this, SLOT(openProject()));
    connect(actionSaveProject_, SIGNAL(triggered(bool)), this, SLOT(saveProject()));

    connect(actionNewMap_, SIGNAL(triggered(bool)), this, SLOT(newMap()));
    connect(actionSaveMap_, SIGNAL(triggered(bool)), this, SLOT(saveMap()));

    connect(actionQuit_, SIGNAL(triggered(bool)), this, SLOT(close()));

    connect(actionUndo_, SIGNAL(triggered(bool)), this, SLOT(undo()));
    connect(actionRedo_, SIGNAL(triggered(bool)), this, SLOT(redo()));

    connect(actionSplitView_, SIGNAL(triggered(bool)), this, SLOT(splitView()));
    connect(actionFly_, SIGNAL(triggered()), this, SLOT(toggleFlyMode()));
    connect(actionPlay_, SIGNAL(triggered()), this, SLOT(playLevel()));

    connect(actionAboutEdddy_, SIGNAL(triggered(bool)), this, SLOT(about()));
}

void MainWindow::retranslateUi()
{
    setWindowTitle(QCoreApplication::translate("MainWindow", "Edddy", nullptr));
    actionQuit_->setText(QCoreApplication::translate("MainWindow", "Quit", nullptr));

    actionNewMap_->setText(QCoreApplication::translate("MainWindow", "New Map...", nullptr));
#if QT_CONFIG(tooltip)
    actionNewMap_->setToolTip(QCoreApplication::translate("MainWindow", "Create new map", nullptr));
#endif

    actionSaveMap_->setText(QCoreApplication::translate("MainWindow", "Save Map", nullptr));
#if QT_CONFIG(tooltip)
    actionSaveMap_->setToolTip(QCoreApplication::translate("MainWindow", "Save active map", nullptr));
#endif // QT_CONFIG(tooltip)

    actionUndo_->setText(QCoreApplication::translate("MainWindow", "Undo", nullptr));
    actionRedo_->setText(QCoreApplication::translate("MainWindow", "Redo", nullptr));

    actionBrush_->setText(QCoreApplication::translate("MainWindow", "Brush", nullptr));
#if QT_CONFIG(tooltip)
    actionBrush_->setToolTip(QCoreApplication::translate("MainWindow", "Brush tool", nullptr));
#endif // QT_CONFIG(tooltip)
    actionFill_->setText(QCoreApplication::translate("MainWindow", "Fill", nullptr));
#if QT_CONFIG(tooltip)
    actionFill_->setToolTip(QCoreApplication::translate("MainWindow", "Fill tool", nullptr));
#endif // QT_CONFIG(tooltip)
    actionDig_->setText(QCoreApplication::translate("MainWindow", "Dig", nullptr));
#if QT_CONFIG(tooltip)
    actionDig_->setToolTip(QCoreApplication::translate("MainWindow", "Dig tool", nullptr));
#endif // QT_CONFIG(tooltip)

    actionOpenProject_->setText(QCoreApplication::translate("MainWindow", "Open Project...", nullptr));
#if QT_CONFIG(tooltip)
    actionOpenProject_->setToolTip(QCoreApplication::translate("MainWindow", "Open project", nullptr));
#endif // QT_CONFIG(tooltip)

    actionLockX_->setText(QCoreApplication::translate("MainWindow", "X", nullptr));
    actionLockY_->setText(QCoreApplication::translate("MainWindow", "Y", nullptr));
    actionLockZ_->setText(QCoreApplication::translate("MainWindow", "Z", nullptr));
#if QT_CONFIG(tooltip)
    actionLockX_->setToolTip(QCoreApplication::translate("MainWindow", "Lock to X-axis", nullptr));
    actionLockY_->setToolTip(QCoreApplication::translate("MainWindow", "Lock to Y-axis", nullptr));
    actionLockZ_->setToolTip(QCoreApplication::translate("MainWindow", "Lock to Z-axis", nullptr));
#endif // QT_CONFIG(tooltip)

    actionAboutEdddy_->setText(QCoreApplication::translate("MainWindow", "About Edddy...", nullptr));
    actionSaveProject_->setText(QCoreApplication::translate("MainWindow", "Save Project", nullptr));
#if QT_CONFIG(tooltip)
    actionSaveProject_->setToolTip(QCoreApplication::translate("MainWindow", "Save current project", nullptr));
#endif // QT_CONFIG(tooltip)
    actionSplitView_->setText(QCoreApplication::translate("MainWindow", "Split View", nullptr));
#if QT_CONFIG(tooltip)
    actionSplitView_->setToolTip(QCoreApplication::translate("MainWindow", "Split active view", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
    actionSplitView_->setStatusTip(QCoreApplication::translate("MainWindow", "Split the active viewport", nullptr));
#endif // QT_CONFIG(statustip)
    actionNewProject_->setText(QCoreApplication::translate("MainWindow", "New Project...", nullptr));
#if QT_CONFIG(tooltip)
    actionNewProject_->setToolTip(QCoreApplication::translate("MainWindow", "Create new project", nullptr));
#endif // QT_CONFIG(tooltip)

    menuFile_->setTitle(QCoreApplication::translate("MainWindow", "File", nullptr));
    menuEdit_->setTitle(QCoreApplication::translate("MainWindow", "Edit", nullptr));
    menuTools_->setTitle(QCoreApplication::translate("MainWindow", "Tools", nullptr));
    menuHelp_->setTitle(QCoreApplication::translate("MainWindow", "Help", nullptr));
    mainToolBar_->setWindowTitle(QCoreApplication::translate("MainWindow", "Toolbar", nullptr));
}
