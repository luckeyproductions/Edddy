/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "blockmap/blockmap.h"
#include "blockmap/gridblock.h"
#include "edddycursor.h"
#include "tools/dig.h"

#include "centers.h"

Centers::Centers(Context* context): Component(context),
    billboards_{},
    blockMap_{ nullptr }
{
}

void Centers::OnNodeSet(Node* node)
{
    if (!node)
        return;


    for (int s{ 0 }; s < 2; ++s)
    {
        BillboardSet* billboardSet{ node_->CreateComponent<BillboardSet>() };
        billboardSet->SetTemporary(true);
        billboardSet->SetSorted(true);
        billboardSet->SetMaterial(RES(Material, s == 0 ? "Materials/Center.xml"
                                                       : "Materials/CenterTransparent.xml" ));
        billboards_.Push(billboardSet);
    }
}

void Centers::OnSceneSet(Scene* scene)
{
    if (!scene)
    {
        blockMap_ = nullptr;
        UnsubscribeFromEvent(E_CURSORSTEP);
    }
    else
    {
        blockMap_ = scene->GetComponent<BlockMap>();
        SubscribeToEvent(E_CURSORSTEP, DRY_HANDLER(Centers, HandleCursorStep));
    }
}

void Centers::HandleCursorStep(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateBillboards();
}

void Centers::UpdateBillboards()
{
    if (!blockMap_)
        return;

    const Vector3 blockSize{ blockMap_->GetBlockSize() };
    const float blockScale{ blockSize.Length() / M_SQRT3 };
    EdddyCursor* cursor{ EdddyCursor::cursor_ };
    const bool digging{ cursor->tool()->GetType() == "Dig" };

    HashSet<IntVector3> centerCoords{};
    if (cursor && !cursor->IsHidden())
    {
        if (!digging)
            centerCoords = blockMap_->GetAllCoords();
        else
            centerCoords = blockMap_->GetTileCornersCoords();
    }

    if (billboards_.Front()->GetNumBillboards() == 0 && centerCoords.Size() == 0)
        return;

    for (BillboardSet* bbs: billboards_)
    {
        bbs->SetNumBillboards(centerCoords.Size());

        unsigned b{ 0u };
        for (const IntVector3& coord: centerCoords)
        {
            Billboard* bb{ bbs->GetBillboard(b) };

            if (digging)
            {
                const HashSet<IntVector3>& pointed{ static_cast<Dig*>(cursor->tool())->affectedCoordinates() };
                const bool active{ pointed.Contains(coord) };
                const Vector3 centerPosition{ coord * blockSize - blockSize * .5f };
                const float distanceToCursor{ cursor->GetNode()->GetWorldPosition().DistanceToPoint(centerPosition) };
                const float size{ blockScale * .07f * (PowN(1.f - Clamp(distanceToCursor * .125f - .25f, 0.f, 1/3.f), 2) +
                                                       active * .25f)};

                bb->enabled_ = distanceToCursor < 7.5f;
                bb->position_ = centerPosition;
                bb->size_ = { size, size };
                bb->color_ = (active ? Color::AZURE.Lerp(Color::CYAN, .23f) : Color::WHITE);
            }
            else if (GridBlock::withinLock(coord))
            {
                const bool active{ coord == cursor->GetCoords() };
                const Vector3 centerPosition{ coord * blockSize };
                const float distanceToCursor{ cursor->GetNode()->GetWorldPosition().DistanceToPoint(centerPosition) };
                const float size{ blockScale * .07f * (PowN(1.f - Clamp(distanceToCursor * .125f - .25f, 0.f, 1/3.f), 2) +
                                                       active * .25f)};

                bb->enabled_ = distanceToCursor < 7.5f;
                bb->position_ = centerPosition;
                bb->size_ = { size, size };
                bb->color_ = (active ? Color::AZURE.Lerp(Color::CYAN, .23f) : Color::WHITE);
            }
            else
            {
                bb->enabled_ = false;
            }

            ++b;
        }

        bbs->Commit();
    }
}
