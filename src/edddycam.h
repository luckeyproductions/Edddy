/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef EDDDYCAM_H
#define EDDDYCAM_H

#include "luckey.h"

using namespace Dry;

class View3D;

#define PITCH_MAX 90.0f
#define PITCH_MIN -90.0f

enum MoveType { MT_ROTATE, MT_MOUSEPAN, MT_KEYPAN, MT_FOV };

class EdddyCam : public Camera
{
    DRY_OBJECT(EdddyCam, Camera);
public:
    EdddyCam(Context *context);
    void OnNodeSet(Node* node) override;
    void HandleMapChange(BlockMap* blockMap);

    void resetPosition(BlockMap* blockMap);
    Quaternion GetRotation() const { return node_->GetRotation(); }
    float GetYaw() const { return GetRotation().EulerAngles().y_; }
    float GetPitch() const { return GetRotation().EulerAngles().x_; }
    Vector3 GetPosition() const { return node_->GetPosition(); }

    void Move(const Vector3& movement, MoveType type = MT_ROTATE);
    float ClampedPitch(float pitchDelta);
    void ToggleOrthogaphic();

    void SetView(View3D* view);

private:
    SharedPtr<View3D> view3d_;
};

#endif // EDDDYCAM_H
