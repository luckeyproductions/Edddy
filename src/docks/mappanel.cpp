/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QAction>
#include <QMenuBar>

#include "../view3d.h"
#include "../mainwindow.h"
#include "../editmaster.h"
#include "../blockmap/blockmap.h"
#include "../dialogs/resizemapdialog.h"
#include "../variantinput.h"
#include "../undostack/mapattributecommand.h"

#include "mappanel.h"

MapPanel::MapPanel(Context* context, QWidget* parent): DryWidget(context, parent),
    nameEdit_{ nullptr },
    widthBox_{ nullptr },
    heightBox_{ nullptr },
    depthBox_{ nullptr },
    resizeMapAction_{ nullptr },
    attributesForm_{ nullptr }
{
    setObjectName("mapPanel");
    setWindowTitle("Map");
    context_->RegisterSubsystem(this);

    QVBoxLayout* mainLayout{ new QVBoxLayout{} };

    QHBoxLayout* mapNameRow{ new QHBoxLayout{} };
    QLabel* mapNameLabel{ new QLabel{ "Name:" } };
    nameEdit_ = new QLineEdit{};
    QPushButton* resetMapNameButton{ new QPushButton{} };
    resetMapNameButton->setIcon(QIcon{ ":/Undo" });
    resetMapNameButton->setToolTip("Restore map name");
    connect(resetMapNameButton, SIGNAL(pressed()), this, SLOT(resetMapName()));
    connect(nameEdit_, SIGNAL(editingFinished()), this, SLOT(handleMapNameEditingFinished()));

    mapNameRow->addWidget(mapNameLabel);
    mapNameRow->addWidget(nameEdit_);
    mapNameRow->addWidget(resetMapNameButton);
    mainLayout->addLayout(mapNameRow);

    addHLine(mainLayout);
    mainLayout->addLayout(CreateMapSizeRow());
    addHLine(mainLayout);

    QHBoxLayout* mapBackgroundRow{ new QHBoxLayout{} };
    QLabel* backgroundLabel{ new QLabel{ "Background:" } };
    QComboBox* backgroundBox{ new QComboBox{} };
    backgroundBox->addItems({ "Color", "CubeMap" });
    backgroundBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    mapBackgroundRow->addWidget(backgroundLabel);
    mapBackgroundRow->addWidget(backgroundBox);
    mapBackgroundRow->addSpacerItem(new QSpacerItem{ 0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum } );
//    mainLayout->addLayout(mapBackgroundRow);

    QHBoxLayout* colorRow{ new QHBoxLayout{} };
    QFrame* colorFrame{ new QFrame{} };
    colorFrame->setFixedSize(64, 64);
    colorFrame->setFrameShape(QFrame::Box);
    colorFrame->setStyleSheet("background-color: " + QColor::fromRgb(0, 0, 0).name() + "; border: 2px inset #333;");

    QVBoxLayout* colorColumn{ new QVBoxLayout{} };
    for (int c{ 0 }; c < 3; ++c)
    {
        QHBoxLayout* sliderRow{ new QHBoxLayout{} };

        QLabel* colorLabel{ new QLabel{} };
        switch (c)
        {
        case 0: colorLabel->setText("R"); break;
        case 1: colorLabel->setText("G"); break;
        case 2: colorLabel->setText("B"); break;
        default: break;
        }

        QSlider* colorSlider{ new QSlider{} };
        colorSlider->setOrientation(Qt::Horizontal);

        sliderRow->addWidget(colorLabel);
        sliderRow->addWidget(colorSlider);
        colorColumn->addLayout(sliderRow);
    }

    colorRow->addWidget(colorFrame);
    colorRow->addLayout(colorColumn);
//    mainLayout->addLayout(colorRow);

    attributesForm_ = new QFormLayout{};
    mainLayout->addLayout(attributesForm_);
    mainLayout->addSpacerItem(new QSpacerItem{ 0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding } );

    setLayout(mainLayout);
    setMinimumSize(192, 192);

    setEnabled(false);
}

QHBoxLayout* MapPanel::CreateMapSizeRow()
{
    MainWindow* mw{ MainWindow::mainWindow_ };
    resizeMapAction_ = mw->mapMenu()->addAction("Resize...");
    connect(resizeMapAction_, SIGNAL(triggered(bool)), this, SLOT(handleResizeMapActionTriggered()));

    QHBoxLayout* mapSizeRow{ new QHBoxLayout() };
    for (int i{ 0 }; i < 4; ++i)
    {
        QVBoxLayout* dimensionColumn{ new QVBoxLayout{} };

        if (i != 3)
        {
            QLabel* dimensionLabel{ new QLabel{} };
            dimensionLabel->setAlignment(Qt::AlignHCenter);
            QSpinBox* dimensionBox{ new QSpinBox{} };
            dimensionBox->setReadOnly(true);
            dimensionBox->setButtonSymbols(QSpinBox::NoButtons);
            dimensionBox->setAlignment(Qt::AlignHCenter);

            switch (i)
            {
            default: break;
            case 0:
                dimensionLabel->setText("Width");
                widthBox_ = dimensionBox;
            break;
            case 1:
                dimensionLabel->setText("Height");
                heightBox_ = dimensionBox;
            break;
            case 2:
                dimensionLabel->setText("Depth");
                depthBox_ = dimensionBox;
            break;
            }

            dimensionColumn->addWidget(dimensionLabel);
            dimensionColumn->addWidget(dimensionBox);
        }
        else
        {
            QPushButton* resizeButton{ new QPushButton{} };
            resizeButton->setIcon(QIcon{ ":/Edit" });
            resizeButton->setToolTip("Resize map");
            dimensionColumn->addWidget(resizeButton);
            dimensionColumn->setAlignment(resizeButton, Qt::AlignBottom);

            connect(resizeButton, SIGNAL(clicked(bool)), resizeMapAction_, SLOT(trigger()));
        }

        mapSizeRow->addLayout(dimensionColumn);
        mapSizeRow->setStretch(i, i != 3);
    }

    return mapSizeRow;
}

void MapPanel::handleMapNameEditingFinished()
{
    BlockMap* activeBlockMap{ EM->GetActiveBlockMap() };
    const QString mapName{ EM->validateMapName(nameEdit_->text()) };

    if (nameEdit_->text() != mapName)
        nameEdit_->setText(mapName);

    const String name{ QD(mapName) };
    if (activeBlockMap->name() != name)
    {
        activeBlockMap->setName(name);
        View3D::updateMapBoxes();
    }
}

void MapPanel::resetMapName()
{
    BlockMap* activeBlockMap{ EM->GetActiveBlockMap() };
    if (!activeBlockMap)
        return;

    if (activeBlockMap->resetName())
    {
        nameEdit_->setText(activeBlockMap->name().CString());
        View3D::updateMapBoxes();
    }
}

void MapPanel::handleResizeMapActionTriggered()
{
    ResizeMapDialog* dialog{ new ResizeMapDialog{ context_, this } };
    dialog->deleteLater();

    updateSizeInfo();
}


void MapPanel::updateInfo()
{
    updateSizeInfo();
    updateAttributeInfo();
}

void MapPanel::updateSizeInfo()
{
    MainWindow* mw{ MainWindow::mainWindow_ };
    BlockMap* activeBlockMap{ EM->GetActiveBlockMap() };

    if (activeBlockMap)
    {
        nameEdit_->setText(  activeBlockMap->name().CString());
        widthBox_->setValue( activeBlockMap->GetMapWidth());
        heightBox_->setValue(activeBlockMap->GetMapHeight());
        depthBox_->setValue( activeBlockMap->GetMapDepth());

        setEnabled(true);
        mw->mapMenu()->setEnabled(true);
    }
    else
    {
        nameEdit_->clear();
        widthBox_->setValue(0);
        heightBox_->setValue(0);
        depthBox_->setValue(0);

        setEnabled(false);
        mw->mapMenu()->setEnabled(false);
    }
}

void MapPanel::updateAttributeInfo()
{
    while (attributesForm_->count())
        attributesForm_->removeRow(0);

    BlockMap* activeBlockMap{ EM->GetActiveBlockMap() };
    if (!activeBlockMap)
        return;

    if (Project* project{ MC->CurrentProject() })
    {
        for (const Attribute& a: project->mapAttributes())
        {
            VariantInput* input{ new VariantInput{} };
            input->setVariant(a.second_);
            attributesForm_->addRow(new QLabel{ DQ(a.first_) }, input);

            connect(input, SIGNAL(valueChanged(const Variant&)), this, SLOT(syncAttribute(const Variant&)));
        }

        for (const Attribute& a: activeBlockMap->attributes())
        {
            for (int r{ 0 }; r < attributesForm_->rowCount(); ++r)
            {
                QLabel* label{ static_cast<QLabel*>(attributesForm_->itemAt(r, QFormLayout::LabelRole)->widget()) };

                if (DQ(a.first_) == label->text())
                {
                    VariantInput* input{ static_cast<VariantInput*>(attributesForm_->itemAt(r, QFormLayout::FieldRole)->widget()) };

                    if (a.second_.GetType() == input->type())
                        input->setVariant(a.second_);
                }
            }
        }
    }
}

void MapPanel::updateAttributeInfo(const String& name)
{
    BlockMap* activeBlockMap{ EM->GetActiveBlockMap() };

    for (int r{ 0 }; r < attributesForm_->rowCount(); ++r)
    {
        QLabel* label{ static_cast<QLabel*>(attributesForm_->itemAt(r, QFormLayout::LabelRole)->widget()) };

        if (DQ(name) == label->text())
        {
            VariantInput* input{ static_cast<VariantInput*>(attributesForm_->itemAt(r, QFormLayout::FieldRole)->widget()) };

            for (const Attribute& a: activeBlockMap->attributes())
            {
                if (name == a.first_ && a.second_.GetType() == input->type())
                    input->setVariant(a.second_);
            }
        }
    }
}

void MapPanel::syncAttribute(const Variant& variant)
{
    for (int r{ 0 }; r < attributesForm_->count(); ++r)
    {
        VariantInput* input{ static_cast<VariantInput*>(attributesForm_->itemAt(r, QFormLayout::FieldRole)->widget()) };

        if (input == sender())
        {
            QLabel* label{ static_cast<QLabel*>(attributesForm_->itemAt(r, QFormLayout::LabelRole)->widget()) };
            const String name{ QD(label->text()) };
            BlockMap* activeBlockMap{ EM->GetActiveBlockMap() };

            MapAttributes attributes{ activeBlockMap->attributes() };
            MapAttributeCommand* command{ new MapAttributeCommand{ activeBlockMap, name } };

            for (unsigned a{ 0u }; a < attributes.Size(); ++a)
            {
                if (name == attributes.At(a).first_)
                    command->setOldState(attributes.At(a).second_);
            }

            command->setNewState(variant);
            activeBlockMap->undoStack().push(command);

            return;
        }
    }
}
