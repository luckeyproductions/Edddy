/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BLOCKSPANEL_H
#define BLOCKSPANEL_H

#include <QSplitter>
#include <QListWidget>
#include <QPushButton>

#include "../blockmap/blockset.h"
#include "../drywidget.h"

class BlocksPanel: public DryWidget
{
    Q_OBJECT
    DRY_OBJECT(BlocksPanel, DryWidget);

public:
    explicit BlocksPanel(Context* context, QWidget* parent = nullptr);

    void updateBlockList();
    int rotationSliderValue();

public slots:
    void handleCurrentBlocksetChanged(Blockset* currentBlockset);
    void updateBlocksetList();

    void handleZoomSliderValueChanged(int value);
    void updateBlockPreviews();

protected:
    void resizeEvent(QResizeEvent* event) override;

private slots:
    void handleBlockListCurrentRowChanged(int currentRow);
    void handleBlocksetListCurrentItemChanged();
    void handleBlocksetListItemChanged(QListWidgetItem* item);
    void handleBlockListItemSelectionChanged();

    void deleteBlockset();
    void editBlock();
    void removeBlock();

    void nextBlockset();
    void previousBlockset();

private:
    void createTopHalf();
    void createBottomHalf();
    void connectSignals();
    void updateButtons();

    QSplitter* splitter_;
    QListWidget* blocksetList_;
    QListWidget* blockList_;

    QPushButton* addBlocksetButton_;
    QPushButton* deleteBlocksetButton_;
    QPushButton* previousBlocksetButton_;
    QPushButton* nextBlocksetButton_;
    QPushButton* addBlockButton_;
    QPushButton* editBlockButton_;
    QPushButton* removeBlockButton_;

    QSlider* zoomSlider_;
    QSlider* rotationSlider_;
};

#endif // BLOCKSPANEL_H
