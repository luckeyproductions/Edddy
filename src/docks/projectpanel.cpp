/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QMenu>
#include <QToolBar>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QFileDialog>

#include "../blockmap/blockmap.h"
#include "../blockmap/blockset.h"
#include "../docks/blockspanel.h"
#include "../editmaster.h"
#include "../project.h"
#include "../view3d.h"
#include "../mainwindow.h"
#include "../dialogs/playdialog.h"
#include "../dialogs/displaynamedialog.h"
#include "../dialogs/mapattributesdialog.h"

#include "projectpanel.h"

ProjectPanel::ProjectPanel(Context* context, QWidget* parent): DryWidget(context, parent),
    projectNameButton_{ new QPushButton{} },
    commandEdit_{ new QLineEdit{} },
    resourcesEdit_{ new QLineEdit{} },
    resourcesResetButton_{ new QPushButton{} },
    mainLayout_{ nullptr },
    line_{ nullptr },
    bottomHalf_{ nullptr },
    resourceBrowser_{ new QTreeWidget{} },
    showAllAction_{ nullptr },
    editableTypeHashes_{ Blockset::GetTypeStatic().ToHash(), BlockMap::GetTypeStatic().ToHash() }
{
    setObjectName("projectPanel");
    setWindowTitle("Project");
    context_->RegisterSubsystem(this);

    mainLayout_ = new QVBoxLayout{};
    mainLayout_->setContentsMargins(5, 2, 5, 5);
    mainLayout_->setSpacing(4);

    QVBoxLayout* topHalf{ new QVBoxLayout{} };
    QFont projectNameLabelFont{ projectNameButton_->font() };
    projectNameLabelFont.setItalic(true);
    projectNameLabelFont.setPointSize(11);
    projectNameButton_->setFont(projectNameLabelFont);
    projectNameButton_->setFlat(true);
    topHalf->addWidget(projectNameButton_);
    connect(projectNameButton_, SIGNAL(clicked(bool)), this, SLOT(onNameButtonClicked()));

    {
        QHBoxLayout* commandRow{ new QHBoxLayout{} };

        QPushButton* runButton{  new QPushButton{} };
        runButton->setIcon(QIcon(":/Play"));
        runButton->setToolTip("Run command");
        commandRow->addWidget(runButton);
        connect(runButton, SIGNAL(clicked(bool)), MainWindow::mainWindow_, SLOT(playGame()));

        commandEdit_->setObjectName("Command");
        commandEdit_->setReadOnly(true);
        commandRow->addWidget(commandEdit_);

        QPushButton* commandEditButton{  new QPushButton{} };
        commandEditButton->setIcon(QIcon(":/Edit"));
        commandEditButton->setToolTip("Edit play command");
        commandEditButton->setProperty("lineedit", QVariant{ QMetaType::QObjectStar, &commandEdit_ });
        commandRow->addWidget(commandEditButton);
        topHalf->addLayout(commandRow);

        connect(commandEditButton,  SIGNAL(clicked(bool)), this, SLOT(onCommandEditButtonClicked()));
    }

    QFormLayout* primaryForm{ new QFormLayout{} };
    primaryForm->setContentsMargins(2, 0, 0, 3);
    {
        QHBoxLayout* resourcesRow{ new QHBoxLayout{} };
        resourcesEdit_->setObjectName("Resources");
        resourcesRow->addWidget(resourcesEdit_);

        QPushButton* resourcesPickButton{  new QPushButton{} };
        resourcesPickButton->setIcon(QIcon(":/Open"));
        resourcesPickButton->setToolTip("Pick resources folder");
        resourcesPickButton->setProperty("lineedit", QVariant{ QMetaType::QObjectStar, &resourcesEdit_ });
        resourcesRow->addWidget(resourcesPickButton);

        resourcesResetButton_->setObjectName("resetResourcesButton");
        resourcesResetButton_->setIcon(QIcon{ ":/Undo" });
        resourcesResetButton_->setToolTip("Revert resources folder");
        resourcesResetButton_->setProperty("lineedit", QVariant{ QMetaType::QObjectStar, &resourcesEdit_ });
        resourcesRow->addWidget(resourcesResetButton_);

        primaryForm->addRow("Resource folder:", resourcesRow);

        connect(resourcesEdit_,        SIGNAL(editingFinished()), this, SLOT(onFolderEditFinished()));
        connect(resourcesPickButton,   SIGNAL(clicked(bool)),     this, SLOT(onFolderPickClicked()));
        connect(resourcesResetButton_, SIGNAL(clicked(bool)),     this, SLOT(onFolderResetClicked()));
    }

    topHalf->addLayout(primaryForm);

    QPushButton* mapAttributesButton{ new QPushButton{ "Edit map attributes..." } };
    topHalf->addWidget(mapAttributesButton);
    connect(mapAttributesButton, SIGNAL(clicked(bool)), this, SLOT(onMapAttributesButtonClicked()));
    mainLayout_->addLayout(topHalf);

    line_ = addHLine(mainLayout_);

    bottomHalf_ = new QVBoxLayout{};

    QToolBar* browserToolBar{ new QToolBar{} };
    showAllAction_ = browserToolBar->addAction("Show all");
    showAllAction_->setCheckable(true);
    bottomHalf_->addWidget(browserToolBar);
    connect(showAllAction_, SIGNAL(triggered(bool)), this, SLOT(updateFolderVisibility()));

    resourceBrowser_->setSelectionMode(QAbstractItemView::ExtendedSelection);
    resourceBrowser_->setDragEnabled(true);
    resourceBrowser_->setHeaderHidden(true);
    bottomHalf_->addWidget(resourceBrowser_);

    resourceBrowser_->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(resourceBrowser_, SIGNAL(customContextMenuRequested(QPoint)),   this, SLOT(showBrowserMenu(QPoint)));
    connect(resourceBrowser_, SIGNAL(itemExpanded(QTreeWidgetItem*)), this, SLOT(updateFolderIcon(QTreeWidgetItem*)));
    connect(resourceBrowser_, SIGNAL(itemCollapsed(QTreeWidgetItem*)), this, SLOT(updateFolderIcon(QTreeWidgetItem*)));
    connect(resourceBrowser_, SIGNAL(itemActivated(QTreeWidgetItem*, int)), this, SLOT(resourceBrowserItemActivated(QTreeWidgetItem*)));

    mainLayout_->addLayout(bottomHalf_);
    mainLayout_->setStretch(2, 3);

    setLayout(mainLayout_);
    setMinimumSize(192, 128);

    setEnabled(false);
}

void ProjectPanel::onFolderEditFinished()
{
    QLineEdit* lineEdit{ qobject_cast<QLineEdit*>(sender()) };
    String name{ QD(lineEdit->objectName()) };
    String attribute{ name  + "Folder" };
    String folder{ lineEdit->text().toStdString().data() };
    String input{ folder };

    while (folder.Find('.') != String::NPOS)
        folder.Erase(folder.Find('.'));

    if (folder.Contains('/') && folder.Length() > 1)
        folder = folder.Split('/').Front().Append('/');

    if (!folder.Contains('/'))
        folder = folder.Append('/');

    if (input != folder)
        lineEdit->setText(folder.CString());

    Project* project{ MC->CurrentProject() };

    if (project->GetAttribute(attribute).GetString() != folder)
        project->SetAttribute(attribute, folder);
}

void ProjectPanel::onFolderPickClicked()
{
    QPushButton* pickButton{ qobject_cast<QPushButton*>(sender()) };
    Project* project{ MC->CurrentProject() };
    QString projectLocation{ project->GetAttribute("Location").ToString().CString() };
    QLineEdit* lineEdit{ qobject_cast<QLineEdit*>(qvariant_cast<QObject*>(pickButton->property("lineedit"))) };
    const String name{ lineEdit->objectName().toStdString().data() };
    const bool resourcesFolder{ name == "Resources" };
    const QString path { QFileDialog::getExistingDirectory(this, tr("Pick folder"), resourcesFolder
                                                           ? projectLocation
                                                           : projectLocation + project->GetAttribute("ResourcesFolder").ToString().CString()) };
    if (path.contains(projectLocation))
    {
        QString folder{ path.split('/').last().append('/') };
        if (lineEdit->text() != folder)
        {
            project->SetAttribute(name + "Folder", folder.toStdString().data());
            lineEdit->setText(folder);
        }
    }
}

void ProjectPanel::onFolderResetClicked()
{
    QPushButton* resetButton{ qobject_cast<QPushButton*>(sender()) };
    Project* project{ MC->CurrentProject() };
    QString name{ resetButton->objectName() };
    name.chop(6);
    name = name.right(name.length() - 5);

    String attribute{ name.toStdString().data() };
    attribute.Append("Folder");

    const String defaultValue{ project->GetAttributeDefault(attribute) };
    project->SetAttribute(attribute, defaultValue);

    QLineEdit* lineEdit{ qobject_cast<QLineEdit*>(qvariant_cast<QObject*>(resetButton->property("lineedit"))) };
    lineEdit->setText(defaultValue.CString());
    updateBrowser();
}

void ProjectPanel::onMapAttributesButtonClicked()
{
    MapAttributesDialog* dialog{ new MapAttributesDialog(context_, this) };
    dialog->deleteLater();
}

void ProjectPanel::updatePanel(bool browser)
{
    if (Project* project{ MC->CurrentProject() })
    {
        const QString displayName{ project->displayName() };
        projectNameButton_->setText((displayName.isEmpty() ? project->trimmedName() : displayName));
        commandEdit_->setText(DQ(project->command()));
        resourcesEdit_->setText(DQ(project->resourceFolder()));
    }
    else
    {
        projectNameButton_->setText("Project name");
    }

    if (browser)
        updateBrowser();
}

void ProjectPanel::updateBrowser()
{
    Project* project{ MC->CurrentProject() };

    if (!project)
    {
        resourceBrowser_->clear();
        return;
    }

    QTreeWidgetItem* selectedItem{ resourceBrowser_->currentItem() };
    const QString selected{ (selectedItem ? selectedItem->data(0, FileName).toString() : "" ) };
    const QStringList expanded{ expandedPaths() };

    resourceBrowser_->clear();

    QTreeWidgetItem* resourcesFolderItem{ new QTreeWidgetItem{} };
    const String resourcesFolder{ project->resourceFolder() };
    const String resourcesPath{ project->location() + resourcesFolder };

    resourcesFolderItem->setText(0, resourcesFolder.Substring(0, resourcesFolder.Length() - 1).CString());

    if (FS->DirExists(resourcesPath))
    {
        resourcesFolderItem->setIcon(0, QIcon(":/Open"));
        updateFolderIcon(resourcesFolderItem);
        resourcesFolderItem->setData(0, FileName, QVariant{ DQ(resourcesPath) });

        buildTree(resourcesFolderItem);
    }
    else
    {
        resourcesFolderItem->setIcon(0, QIcon(":/MissingFolder"));
    }

    resourceBrowser_->addTopLevelItem(resourcesFolderItem);
    resourcesFolderItem->setExpanded(true);

    for (QTreeWidgetItem* item: allItems())
    {
        const QString fileName{ item->data(0, FileName).toString() };
        if (expanded.contains(fileName))
            item->setExpanded(true);

        if (fileName == selected)
            resourceBrowser_->setCurrentItem(item);
    }

    GetSubsystem<BlocksPanel>()->updateBlocksetList();
    updateFolderVisibility();
}

void ProjectPanel::focusBrowser()
{
    if (resourceBrowser_->isVisible() && resourceBrowser_->topLevelItemCount() != 0)
    {
        resourceBrowser_->setCurrentItem(resourceBrowser_->topLevelItem(0));
        resourceBrowser_->setFocus(Qt::OtherFocusReason);
    }
}

bool ProjectPanel::isShowingAll() const
{
    return showAllAction_->isChecked();
}

void ProjectPanel::buildTree(QTreeWidgetItem* treeItem)
{
    Project* project{ MC->CurrentProject() };
    const String path{ QD(treeItem->data(0, FileName).toString()) };

    Vector<String> scanResults{};
    FS->ScanDir(scanResults, path, "*.*", SCAN_DIRS, false);
    Sort(scanResults.Begin(), scanResults.End());

    for (const String& subFolder: scanResults)
    {
        if (subFolder.Front() == '.')
            continue;


        const QString fullSubPath{ DQ(AddTrailingSlash(AddTrailingSlash(path) + subFolder)) };
        QTreeWidgetItem* subFolderItem{ new QTreeWidgetItem{} };

        subFolderItem->setText(0, DQ(subFolder));
        subFolderItem->setData(0, FileName, fullSubPath);
        subFolderItem->setToolTip(0, DQ(project->localizedPath(QD(fullSubPath))));
        updateFolderIcon(subFolderItem);

        buildTree(subFolderItem);
        treeItem->addChild(subFolderItem);
    }

    FS->ScanDir(scanResults, path, "*.*", SCAN_FILES, false);
    Sort(scanResults.Begin(), scanResults.End());

    for (const String& fileName: scanResults)
    {
        QTreeWidgetItem* fileItem{ new QTreeWidgetItem{} };
        const String fullPath{ AddTrailingSlash(path) + fileName };
        const String localPath{ project->localizedPath(fullPath) };
        const String extension{ GetExtension(fileName) };
        QString iconName{ ":/New" };

        fileItem->setText(0, DQ(fileName));
        fileItem->setToolTip(0, DQ(localPath));

        if (extension == ".emp")
        {
            fileItem->setData(0, DryTypeHash, QVariant(BlockMap::GetTypeStatic().ToHash()));
            iconName = ":/Map";
        }
        else if (extension == ".ebs")
        {
            fileItem->setData(0, DryTypeHash, QVariant(Blockset::GetTypeStatic().ToHash()));
            iconName = ":/Blockset";

            EM->LoadBlockset(fullPath);
        }
        else if (extension == ".png" || extension == ".jpg" || extension == ".dds")
        {
            fileItem->setData(0, DryTypeHash, QVariant(Texture2D::GetTypeStatic().ToHash()));
            iconName = ":/Image";
        }
        else if (extension == ".ogg" || extension == ".wav")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Sound::GetTypeStatic().ToHash() });
            iconName = ":/Sound";
        }
        else if (extension == ".mdl")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Model::GetTypeStatic().ToHash() });
            iconName = ":/Model";
            RES(Model, localPath);
        }
        else if (extension == ".ani")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Animation::GetTypeStatic().ToHash() });
            iconName = ":/Animation";
        }
        else if (extension == ".as")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ ScriptFile::GetTypeStatic().ToHash() });
            iconName = ":/Script";
        }
        else if (extension == ".otf" || extension == ".ttf")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Font::GetTypeStatic().ToHash() });
            iconName = ":/Font";
        }
        else if (extension == ".json")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ JSONFile::GetTypeStatic().ToHash() });
        }
        else if (extension == ".xml")
        {
            SharedPtr<XMLFile> xmlFile{ GetSubsystem<ResourceCache>()->GetTempResource<XMLFile>(fullPath) };

            if (xmlFile)
            {
                String rootName{ xmlFile->GetRoot().GetName() };

                if (rootName == "material")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Material::GetTypeStatic().ToHash() });
                    iconName = ":/Material";
                    RES(Material, localPath);
                }
                else if (rootName == "technique")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Technique::GetTypeStatic().ToHash() });
                }
                else if (rootName == "scene")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Scene::GetTypeStatic().ToHash() });
                    iconName = ":/SceneResource";
                }
                else if (rootName == "node")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Node::GetTypeStatic().ToHash() });
//                    prefabs_.push_back(toQString(fullFileName));
                }
                else if (rootName == "particleeffect")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ ParticleEffect::GetTypeStatic().ToHash() });
                    iconName = ":/ParticleEffect";
                }
                else
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ XMLFile::GetTypeStatic().ToHash() });
                }
            }
        }
        else
        {
            fileItem->setData(0, DryTypeHash, QVariant{ File::GetTypeStatic().ToHash() });
        }

        fileItem->setData(0, FileName, DQ(fullPath));
        fileItem->setIcon(0, QIcon{ iconName });

        treeItem->addChild(fileItem);
    }
}

void ProjectPanel::onNameButtonClicked()
{
    DisplayNameDialog* dialog{ new DisplayNameDialog(context_, this) };
    dialog->deleteLater();
}

void ProjectPanel::onCommandEditButtonClicked()
{
    PlayDialog* dialog{ new PlayDialog{ context_, this } };
    dialog->deleteLater();
}

QStringList ProjectPanel::expandedPaths() const
{
    QStringList expandedPaths{};
    for (QTreeWidgetItem* item: allItems())
    {
        if (item->isExpanded())
            expandedPaths.push_back(item->data(0, FileName).toString());
    }

    return expandedPaths;
}

QList<QTreeWidgetItem*> ProjectPanel::allItems(bool onlyTopLevel) const
{
    QList<QTreeWidgetItem*> treeItems{};

    for (int i{ 0 }; i < resourceBrowser_->topLevelItemCount(); ++i)
    {
        if (QTreeWidgetItem* item{ resourceBrowser_->topLevelItem(i) })
        {
            if (!onlyTopLevel)
                collectChildren(treeItems, item, true);

            treeItems.push_back(item);
        }
    }

    return treeItems;
}

void ProjectPanel::collectChildren(QList<QTreeWidgetItem*>& children, QTreeWidgetItem* item, bool recursive) const
{
    for (int c{ 0 }; c < item->childCount(); ++c)
    {
        QTreeWidgetItem* childItem{ item->child(c) };
        children.push_back(childItem);

        if (recursive)
            collectChildren(children, childItem, true);
    }
}

void ProjectPanel::updateFolderIcon(QTreeWidgetItem* item)
{
    item->setIcon(0, QIcon{ ":/" + QString{ item->isExpanded() ? "Open" : "Folder" } });
}

void ProjectPanel::hideFolders(QList<QTreeWidgetItem*> items)
{
    Project* project{ MC->CurrentProject() };
    QStringList& hiddenFolders{ project->hiddenFolders() };

    for (QTreeWidgetItem* item: items)
    {
        QList<QTreeWidgetItem*> children{ item };
        collectChildren(children, item, true);

        for (QTreeWidgetItem* child: children)
        {
            const QString path{ child->data(0, FileName).toString() };
            if (child->data(0, DryTypeHash).isNull() && !hiddenFolders.contains(path))
                hiddenFolders.push_back(path);
        }
    }

    project->saveSettings();
    updateFolderVisibility();
}

void ProjectPanel::unhideFolders()
{
    Project* project{ MC->CurrentProject() };
    QStringList& hiddenFolders{ project->hiddenFolders() };

    for (QTreeWidgetItem* item: resourceBrowser_->selectedItems())
    {
        QList<QTreeWidgetItem*> children{ item };
        collectChildren(children, item, true);

        for (QTreeWidgetItem* child: children)
        {
            const QString path{ child->data(0, FileName).toString() };
            if (child->data(0, DryTypeHash).isNull() && hiddenFolders.contains(path))
                hiddenFolders.removeOne(path);
        }
    }

    project->saveSettings();
    updateFolderVisibility();
}

void ProjectPanel::updateFolderVisibility()
{
    Project* project{ MC->CurrentProject() };
    const QStringList& hiddenFolders{ project->hiddenFolders() };

    QList<QTreeWidgetItem*> topLevelItems{ allItems(true) };
    QList<QTreeWidgetItem*> items{ allItems() };
    const bool hide{ !showAllAction_->isChecked() };

    for (QTreeWidgetItem* item: items)
    {
        if (!topLevelItems.contains(item))
            item->setHidden(hide);
    }

    if (hide)
    {
        for (QTreeWidgetItem* item: items)
        {
            if (item->data(0, DryTypeHash).isNull() && !hiddenFolders.contains(item->data(0, FileName).toString()))
            {
                QTreeWidgetItem* parent{ item->parent() };
                while (parent)
                {
                    parent->setHidden(false);
                    parent = parent->parent();
                }

                QList<QTreeWidgetItem*> directChildren{};
                collectChildren(directChildren, item, false);
                for (QTreeWidgetItem* child: directChildren)
                {
                    if (!child->data(0, DryTypeHash).isNull())
                        child->setHidden(false);
                }

                item->setHidden(false);
            }
        }
    }

    resourceBrowser_->repaint();
}

void ProjectPanel::resourceBrowserItemActivated(QTreeWidgetItem* item)
{
    const unsigned itemType{ item->data(0, DryTypeHash).toUInt() };
    const String filename{ QD(item->data(0, FileName).toString()) };

    if (itemType == BlockMap::GetTypeStatic().ToHash())
        EM->LoadMap(filename);
    else if (itemType == Blockset::GetTypeStatic().ToHash())
        EM->SetCurrentBlockset(EM->LoadBlockset(filename));
}

void ProjectPanel::showBrowserMenu(const QPoint& pos)
{
    Project* project{ MC->CurrentProject() };
    const QStringList& hiddenFolders{ project->hiddenFolders() };

    const QPoint globalPos{ resourceBrowser_->mapToGlobal(pos) };
    QMenu browserMenu{};

    QList<QTreeWidgetItem*> selection{ resourceBrowser_->selectedItems() };
    QTreeWidgetItem* current{ resourceBrowser_->currentItem() };

    int visibileFolderCount{};
    int hiddenFolderCount{};
    for (QTreeWidgetItem* item: selection)
    {
        if (item->data(0, DryTypeHash).isNull())
        {
            if (!item->parent())
                selection.removeOne(item);
            else if (hiddenFolders.contains(item->data(0, FileName).toString()))
                ++hiddenFolderCount;
            else
                ++visibileFolderCount;
        }
    }

    if (current)
    {
        if (visibileFolderCount > 0)
        {
            QString hideText{ "Hide folder" };
            if (visibileFolderCount > 1)
                hideText += "s";

            browserMenu.addAction(QIcon(":/Hide"), hideText);
        }

        if (hiddenFolderCount > 0)
        {
            QString unhideText{ "Unhide folder" };
            if (hiddenFolderCount > 1)
                unhideText += "s";

            browserMenu.addAction(QIcon(":/Unhide"), unhideText, this, SLOT(unhideFolders()));
        }

        if (editableTypeHashes_.contains(current->data(0, DryTypeHash).toUInt()))
        {
            browserMenu.addAction(QIcon(":/Edit"), "Edit", this, SLOT(editBrowserItem()));
        }

        browserMenu.addAction(QIcon(":/Delete"), "Delete", this, SLOT(deleteSelectedFile()));
    }


    if (!browserMenu.actions().isEmpty())
        browserMenu.addSeparator();

    browserMenu.addAction(QIcon(":/Material"), "New Material");
    browserMenu.addAction(QIcon(":/Map"),      "New Map",      MainWindow::mainWindow_, SLOT(newMap()));
    browserMenu.addAction(QIcon(":/Blockset"), "New Blockset", MainWindow::mainWindow_, SLOT(newBlockset()));

    QList<QAction*> actions{ browserMenu.actions() };
    for (int a{ 0 }; a < actions.size(); ++a)
    {
        if (actions.at(a)->text() == "New Material")
            actions.at(a)->setEnabled(false);
    }

    QAction* pickedAction{ browserMenu.exec(globalPos) };
    if (pickedAction && pickedAction->text().startsWith("Hide"))
        hideFolders(selection);
}

void ProjectPanel::editBrowserItem()
{
    resourceBrowserItemActivated(resourceBrowser_->currentItem());
}

void ProjectPanel::deleteSelectedFile()
{
    bool deleted{ false };
    QTreeWidgetItem* item{ resourceBrowser_->currentItem() };
    const String filename{ QD(item->data(0, FileName).toString()) };

    if (item->text(0).contains('.'))
    {
        QMessageBox* confirm{ new QMessageBox(this) };
        confirm->setWindowTitle("Delete file");
        confirm->setWindowIcon(QIcon(":/Edddy"));
        confirm->setIcon(QMessageBox::Warning);

        QString file{ item->text(0) };
        if (file.right(1) == '*')
            file.chop(1);

        confirm->setText("Are you sure you want to delete " + file + "?");
        confirm->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        confirm->setDefaultButton(QMessageBox::Cancel);

        if (confirm->exec() == QMessageBox::Cancel)
            return;


        const unsigned itemType{ item->data(0, DryTypeHash).toUInt() };
        const String filename{ QD(item->data(0, FileName).toString()) };

        if (itemType == BlockMap::GetTypeStatic().ToHash())
            deleted = EM->DeleteBlockMap(filename);
        else if (itemType == Blockset::GetTypeStatic().ToHash())
            deleted = EM->DeleteBlockset(filename);
        else if (filename.Contains('/') && FS->FileExists(filename))
            deleted = FS->Delete(filename);
    }

    if (deleted)
        updateBrowser();
}

void ProjectPanel::resizeEvent(QResizeEvent* /*event*/)
{
    if ((width() * 1.f / height()) < 2.f)
    {
        mainLayout_->setDirection(QBoxLayout::TopToBottom);
        line_->setFrameShape(QFrame::HLine);
        mainLayout_->setStretch(0, 0);
    }
    else
    {
        mainLayout_->setDirection(QBoxLayout::LeftToRight);
        line_->setFrameShape(QFrame::VLine);
        mainLayout_->setStretch(0, 2);
    }
}
