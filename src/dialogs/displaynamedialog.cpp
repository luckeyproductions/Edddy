/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QIcon>
#include "../project.h"

#include "displaynamedialog.h"

DisplayNameDialog::DisplayNameDialog(Context* context, QWidget* parent): QDialog(parent), Object(context),
    buttonBox_{ new QDialogButtonBox{ QDialogButtonBox::Ok | QDialogButtonBox::Cancel } },
    nameEdit_{ nullptr }
{
    setWindowTitle("Display name");
    setWindowIcon(QIcon{ ":/Edddy" });

    QVBoxLayout* layout{ new QVBoxLayout{ this } };

    QHBoxLayout* nameRow{ new QHBoxLayout{} };
    nameEdit_ = new QLineEdit{};
    nameEdit_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    nameEdit_->setMinimumWidth(64);
    nameEdit_->setAlignment(Qt::AlignHCenter);
    Project* p{ MC->CurrentProject() };
    nameEdit_->setText(p->displayName());
    nameRow->addWidget(nameEdit_);

    layout->addLayout(nameRow);
    layout->addWidget(buttonBox_);

    connect(nameEdit_, SIGNAL(editingFinished()), this, SLOT(onNameEditFinished()));
    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    exec();
}

void DisplayNameDialog::onNameEditFinished()
{
    nameEdit_->setText(nameEdit_->text().trimmed());
}

void DisplayNameDialog::accept()
{
    Project* p{ MC->CurrentProject() };
    const QString oldName{ p->displayName() };
    const QString newName{ nameEdit_->text() };

    if (oldName != newName)
        p->SetAttribute("DisplayName", QD(newName));

    QDialog::accept();
}
