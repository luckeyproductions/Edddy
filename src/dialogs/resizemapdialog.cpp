/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QIcon>
#include "../blockmap/blockmap.h"
#include "../view3d.h"

#include "resizemapdialog.h"

ResizeMapDialog::ResizeMapDialog(Context* context, QWidget* parent): QDialog(parent), Object(context),
    buttonBox_{ new QDialogButtonBox{ QDialogButtonBox::Ok | QDialogButtonBox::Cancel } },
    mapWidthBox_ { nullptr },
    mapHeightBox_{ nullptr },
    mapDepthBox_ { nullptr }
{
    setWindowTitle("Resize Map");
    setWindowIcon(QIcon{ ":/Map" });

    BlockMap* activeMap{ nullptr };
    if (View3D::Active())
        activeMap = View3D::Active()->GetBlockMap();

    if (!activeMap)
        reject();

    QVBoxLayout* layout{ new QVBoxLayout{ this } };
    layout->setSizeConstraint(QLayout::SetFixedSize);

    QFormLayout* form{ new QFormLayout{} };
    form->setLabelAlignment(Qt::AlignRight);

    QHBoxLayout* mapRow{ new QHBoxLayout{} };
    for (int i{ 0 }; i < 3; ++i)
    {
        QSpinBox* spinBox{ new QSpinBox{} };
        spinBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        spinBox->setMinimumWidth(64);
        spinBox->setMinimum(1);
        spinBox->setMaximum(256);
        spinBox->setAlignment(Qt::AlignRight);

        QString labelText{};

        switch (i) {
        case 0:
            labelText = "X";
            mapWidthBox_ = spinBox;
            spinBox->setValue(activeMap->GetMapWidth());
        break;
        case 1:
            labelText = "Y";
            mapHeightBox_ = spinBox;
            spinBox->setValue(activeMap->GetMapHeight());
        break;
        case 2:
            labelText = "Z";
            mapDepthBox_ = spinBox;
            spinBox->setValue(activeMap->GetMapDepth());
        break;
        default:
        break;
        }

        mapRow->addWidget(new QLabel{ labelText });
        mapRow->addWidget(spinBox);
    }
    form->addRow("Map Size:", mapRow);

    layout->addLayout(form);
    layout->addWidget(buttonBox_);

    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    exec();
}

void ResizeMapDialog::accept()
{
    View3D::Active()->GetBlockMap()->SetMapSize(mapWidthBox_->value(),
                                                mapHeightBox_->value(),
                                                mapDepthBox_->value());

    QDialog::accept();
}
