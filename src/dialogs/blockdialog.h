/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef BLOCKDIALOG_H
#define BLOCKDIALOG_H

#include "../luckey.h"
#include "../blocklistitem.h"
#include "../blockmap/block.h"

#include <QDialog>
#include <QDialogButtonBox>
#include <QListWidget>
#include <QSplitter>
#include <QHBoxLayout>
#include <QLabel>

enum AddMode{ AM_BLOCK, AM_TILE, AM_TRACK };

class BlockDialog: public QDialog, public Object
{
    Q_OBJECT
    DRY_OBJECT(BlockDialog, Object);

public:
    explicit BlockDialog(Context* context, Vector<Block*> blocks, QWidget* parent = nullptr);

protected:
    void showEvent(QShowEvent* event) override;

private slots:
    void accept() override;
    void modeChanged(int mode);
    void selectedModelChanged();
    void selectedMaterialChanged();
    void updateMaterialButtons();

private:
    void setNumPreviewItems(unsigned previews);
    QSize previewItemSize() const { return { previewIconSize_, previewIconSize_ + lineHeight_ }; }
    void updateOkButton();
    void populateModelList(const StringVector& hiddenFolders);
    void populateMaterialList(const StringVector& hiddenFolders);

    AddMode mode() const;

    Vector<Block*> blocksIn_;
    HashSet<int> types_;
    Vector<Block*> previewBlocks_;
    Vector<BlockListItem*> previewItems_;
    HashMap<unsigned, SharedPtr<Material>> newBlockMaterials_;
    int previewIconSize_;
    int lineHeight_;

    QTabBar* modeTabs_;
    QSplitter* splitter_;
    QListWidget* previewList_;
    QListWidget* modelList_;
    QListWidget* materialList_;
    QLabel* materialLabel_;
    QWidget* materialButtons_;
    QDialogButtonBox* buttonBox_;
};

#endif // BLOCKDIALOG_H
