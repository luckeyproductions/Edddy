/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QComboBox>

#include "variantinput.h"

VariantInput::VariantInput(QWidget* parent): QWidget(parent),
    negative_{ true },
    type_{ VAR_NONE },
    var_{},
    layout_{ new QHBoxLayout{} }
{
    layout_->setMargin(0);
    layout_->setContentsMargins(0, 0, 0, 0);
    setFocusPolicy(Qt::TabFocus);
    setLayout(layout_);
}

void VariantInput::setVariant(const Variant& variant)
{
    if (var_ == variant)
        return;

    var_ = variant;
    updateType();
    updateValue();
}

void VariantInput::setType(VariantType type)
{
    if (type_ == type)
        return;

    var_ = Variant{ type, "" };
    updateType();
}

void VariantInput::typeChanged()
{
    const VariantType type{ static_cast<VariantType>(static_cast<QComboBox*>(sender())->currentData().toInt()) };
    setType(type);
}

void VariantInput::updateType()
{
    if (var_.GetType() == type_)
        return;

    while (QLayoutItem* item{ layout()->takeAt(0) })
    {
        layout()->removeItem(item);
        delete item->widget();
        delete item;
    }

    switch (var_.GetType())
    {
    default: break;
    case VAR_INT:   case VAR_INT64:  addSpinBoxes(1, true);                      break;
    case VAR_BOOL:                   addCheckBox();                              break;
    case VAR_FLOAT: case VAR_DOUBLE: addSpinBoxes(1, false);                     break;
    case VAR_VECTOR2:                setToolTipsVector(addSpinBoxes(2, false));  break;
    case VAR_VECTOR3:                setToolTipsVector(addSpinBoxes(3, false));  break;
    case VAR_VECTOR4:                setToolTipsVector(addSpinBoxes(4, false));  break;
    case VAR_STRING:                 addLineEdit();                              break;
    case VAR_QUATERNION:             setToolTipsQuaternion(addSpinBoxes(4, -1)); break;
    case VAR_INTRECT:                setToolTipsRect(  addSpinBoxes(4, true));     break;
    case VAR_INTVECTOR2:             setToolTipsVector(addSpinBoxes(2, true));   break;
    case VAR_INTVECTOR3:             setToolTipsVector(addSpinBoxes(3, true));   break;
    case VAR_RECT:                   setToolTipsRect(  addSpinBoxes(4, false));    break;
    }

    if (!layout_->isEmpty())
        setFocusProxy(layout_->itemAt(0)->widget());

    type_ = var_.GetType();
}

void VariantInput::addCheckBox()
{
    QCheckBox* checkBox{ new QCheckBox{} };
    layout_->addWidget(checkBox);
    connect(checkBox, SIGNAL(toggled(bool)), this, SLOT(inputChanged()));
}

void VariantInput::addLineEdit()
{
    QLineEdit* lineEdit{ new QLineEdit{} };
    layout_->addWidget(lineEdit);
    connect(lineEdit, SIGNAL(editingFinished()), this, SLOT(inputChanged()));
}

PODVector<QAbstractSpinBox*> VariantInput::addSpinBoxes(int count, bool integer)
{
    PODVector<QAbstractSpinBox*> spinBoxes{};

    for (int i{ 0 }; i < count; ++i)
    {
        QAbstractSpinBox* spinBox{ nullptr };

        if (integer)
        {
            QSpinBox* intBox{ new QSpinBox{} };
            intBox->setMinimum(M_MIN_INT * negative_);
            intBox->setMaximum(M_MAX_INT);

            spinBox = intBox;
        }
        else
        {
            ShortDoubleSpinBox* floatBox{ new ShortDoubleSpinBox{} };
            floatBox->setMinimum(-M_LARGE_VALUE * negative_);
            floatBox->setMaximum( M_LARGE_VALUE);

            spinBox = floatBox;
        }

        layout_->addWidget(spinBox);
        spinBoxes.Push(spinBox);
        connect(spinBox, SIGNAL(editingFinished()), this, SLOT(inputChanged()));
    }

    setTabOrder(spinBoxes.Front(), spinBoxes.Back());

    return spinBoxes;
}

void VariantInput::setToolTipsVector(const PODVector<QAbstractSpinBox*>& spinBoxes)
{
    for (unsigned i{ 0 }; i < spinBoxes.Size(); ++i) switch (i)
    {
    default: break;
    case 0u: spinBoxes.At(i)->setToolTip((negative_ ? "X" : "Width")); break;
    case 1u: spinBoxes.At(i)->setToolTip((negative_ ? "Y" : "Height")); break;
    case 2u: spinBoxes.At(i)->setToolTip((negative_ ? "Z" : "Depth")); break;
    case 3u: spinBoxes.At(i)->setToolTip((negative_ ? "W" : "")); break;
    }
}

void VariantInput::setToolTipsQuaternion(const PODVector<QAbstractSpinBox*>& spinBoxes)
{
    for (unsigned i{ 0 }; i < spinBoxes.Size(); ++i) switch (i)
    {
    default: break;
    case 0u: spinBoxes.At(i)->setToolTip("W"); break;
    case 1u: spinBoxes.At(i)->setToolTip("X"); break;
    case 2u: spinBoxes.At(i)->setToolTip("Y"); break;
    case 3u: spinBoxes.At(i)->setToolTip("Z"); break;
    }
}

void VariantInput::setToolTipsRect(const PODVector<QAbstractSpinBox*>& spinBoxes)
{
    for (unsigned i{ 0u }; i < spinBoxes.Size(); ++i) switch (i)
    {
    default: break;
    case 0u: spinBoxes.At(i)->setToolTip("Min X"); break;
    case 1u: spinBoxes.At(i)->setToolTip("Min Y"); break;
    case 2u: spinBoxes.At(i)->setToolTip("Max X"); break;
    case 3u: spinBoxes.At(i)->setToolTip("Max Y"); break;
    }
}

void VariantInput::updateValue()
{
    switch (var_.GetType())
    {
    default: break;
    case VAR_INT:        { int v{   var_.GetInt() }; setInt(&v, 1u); }      break;
    case VAR_BOOL:       setBool(   var_.GetBool());                        break;
    case VAR_FLOAT:      { float v{ var_.GetFloat() }; setFloat(&v, 1u); }  break;
    case VAR_DOUBLE:     setDouble( var_.GetDouble());                      break;
    case VAR_VECTOR2:    setFloat(  var_.GetVector2().Data(), 2u);          break;
    case VAR_VECTOR3:    setFloat(  var_.GetVector3().Data(), 3u);          break;
    case VAR_VECTOR4:    setFloat(  var_.GetVector4().Data(), 4u);          break;
    case VAR_STRING:     setString( var_.GetString());                      break;
    case VAR_QUATERNION: setFloat(  var_.GetQuaternion().Data(), 4u);       break;
    case VAR_INTRECT:    setInt(    var_.GetIntRect().Data(), 4u);          break;
    case VAR_INTVECTOR2: setInt(    var_.GetIntVector2().Data(), 2u);       break;
    case VAR_INTVECTOR3: setInt(    var_.GetIntVector3().Data(), 3u);       break;
    case VAR_RECT:       setFloat(  var_.GetRect().Data(), 4u);             break;
    }
}

void VariantInput::inputChanged()
{
    const Variant oldVar{ var_ };

    switch (var_.GetType())
    {
    default: break;
    case VAR_INT:        var_ = Variant{ getInt()        }; break;
    case VAR_BOOL:       var_ = Variant{ getBool()       }; break;
    case VAR_FLOAT:      var_ = Variant{ getFloat()      }; break;
    case VAR_DOUBLE:     var_ = Variant{ getDouble()     }; break;
    case VAR_VECTOR2:    var_ = Variant{ getVector2()    }; break;
    case VAR_VECTOR3:    var_ = Variant{ getVector3()    }; break;
    case VAR_VECTOR4:    var_ = Variant{ getVector4()    }; break;
    case VAR_STRING:     var_ = Variant{ getString()     }; break;
    case VAR_QUATERNION: var_ = Variant{ getQuaternion() }; break;
    case VAR_INTRECT:    var_ = Variant{ getIntRect()    }; break;
    case VAR_INTVECTOR2: var_ = Variant{ getIntVector2() }; break;
    case VAR_INTVECTOR3: var_ = Variant{ getIntVector3() }; break;
    case VAR_RECT:       var_ = Variant{ getRect()       }; break;
    }

    if (var_ != oldVar)
        emit valueChanged(var_);
}
