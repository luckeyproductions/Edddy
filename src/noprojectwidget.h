/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NOPROJECTWIDGET_H
#define NOPROJECTWIDGET_H

#include "luckey.h"

#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>

#include <QWidget>

class NoProjectWidget: public QWidget, public Object
{
    Q_OBJECT
    DRY_OBJECT(NoProjectWidget, Object);

public:
    explicit NoProjectWidget(Dry::Context* context, QWidget* parent = nullptr);

public slots:
    void updateRecentList();
    
signals:
    void newProject();
    void openProject(QString name = "");

protected:
    void resizeEvent(QResizeEvent* event) override;

private:
    void createRecentHeader();
    QPushButton*createRecentProjectButton(const QString& fileName, const QString& projectName) const;

    QVBoxLayout* mainLayout_;
    QLabel* logo_;
    std::pair<QLabel*, QFrame*> recentHeader_;
    std::vector<QPushButton*> recentButtons_;
};

#endif // NOPROJECTWIDGET_H
