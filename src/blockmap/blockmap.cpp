/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDir>

#include "../mainwindow.h"
#include "../inputmaster.h"
#include "../edddycursor.h"
#include "../editmaster.h"
#include "../view3d.h"
#include "../centers.h"
#include "../docks/mappanel.h"
#include "tileinstance.h"
#include "blockset.h"

#include "blockmap.h"

BlockMap::BlockMap(Context* context): QObject(), Component(context),
    name_{},
    savedName_{},
    fullPath_{},
    savedFullPath_{},
    attributes_{},
    centerBillboards_{ nullptr },
    gridLayers_{},
    scene_{ nullptr },
    zone_{ nullptr },
    corners_{},
    mapSize_{},
    undoStack_{},
    blocksetUseCount_{}
{
    connect(&undoStack_, SIGNAL(cleanChanged(bool)), MW, SLOT(handleFileModifiedStateChange()));
}

const QColor BlockMap::GetBackgroundQColor() const
{
    const Color color{ zone_->GetFogColor() };
    const QColor backgroundColor{ RoundToInt(color.r_ * 255),
                                  RoundToInt(color.g_ * 255),
                                  RoundToInt(color.b_ * 255) };

    return backgroundColor;
}

void BlockMap::UpdateViews() const
{
    for (View3D* v: View3D::views_)
    {
        if (v->GetBlockMap() == this)
            v->UpdateView();
    }
}

void BlockMap::OnNodeSet(Node* node)
{
    if (!node)
        return;

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    scene_ = static_cast<Scene*>(node);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<DebugRenderer>();

    Node* centersNode{ scene_->CreateChild("CENTERS") };
    centersNode->SetTemporary(true);
    centerBillboards_ = centersNode->CreateComponent<Centers>();

    sideGroup_ = scene_->CreateComponent<StaticModelGroup>();
    sideGroup_->SetModel(cache->GetTempResource<Model>("Models/Plane.mdl"));
    sideGroup_->SetMaterial(cache->GetTempResource<Material>("Materials/TransparentGlow.xml"));

    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition({ -.5f, 5.0f, -2.0f });
    lightNode->LookAt(Vector3::ZERO);
    Light* previewLight{ lightNode->CreateComponent<Light>() };
    previewLight->SetLightType(LIGHT_DIRECTIONAL);

    zone_ = node_->CreateComponent<Zone>();
    zone_->SetBoundingBox(BoundingBox(-Vector3::ONE * 10e9, Vector3::ONE * 10e9));
    zone_->SetAmbientColor({ .13f, .23f, .42f });
    zone_->SetFogStart(2048.f);
    zone_->SetFogEnd(4096.f);
    zone_->SetFogColor(Color::BLACK);

    CreateCorners();
}

void BlockMap::CreateCorners()
{
    for (int c{ 0 }; c < 8; ++c)
    {
        Node* cornerNode{ scene_->CreateChild("MapCorner") };

        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        StaticModel* cornerModel{ cornerNode->CreateComponent<StaticModel>() };
        cornerModel->SetModel(cache->GetTempResource<Model>("Models/Corner.mdl"));
        cornerModel->SetMaterial(cache->GetTempResource<Material>("Materials/CornerInactive.xml"));

        corners_.Push(cornerNode);
    }
}
void BlockMap::UpdateCorners()
{
    for (unsigned i{ 0 }; i < corners_.Size(); ++i)
    {
        Node* c{ corners_.At(i) };
        c->SetPosition(Vector3(-.5f + 1.f *  (i % 2),
                               -.5f + 1.f * ((i / 2) % 2),
                               -.5f + 1.f *  (i / 4))
                       * Vector3(mapSize_) * blockSize_ + GetCenter() - .5f * blockSize_);

        Vector3 pos{ c->GetPosition() };
        c->SetScale(blockSize_ * Vector3{ Sign(pos.x_) * Sign(pos.y_) * Sign(pos.z_), 1.f, 1.f });
        c->LookAt(Vector3::FORWARD * pos.z_, Vector3::UP * pos.y_, TS_LOCAL);
    }
}

bool BlockMap::LoadXML(const XMLElement &source)
{
    if (!Serializable::LoadXML(source))
        return false;

    SetBlockSize(source.GetVector3("block_size"));
    SetMapSize(source.GetIntVector3("map_size"));

    XMLElement attributesElem{ source.GetChild("attributes")};
    if (!attributesElem.IsNull())
        loadAttributes(attributesElem);

    XMLElement blockSetXML{ source.GetChild("blockset") };

    while (!blockSetXML.IsNull())
    {
        String blocksetPath{ blockSetXML.GetAttribute("name") };
        const bool relative{ blocksetPath.StartsWith("./") || blocksetPath.StartsWith("../") };
        String fullResourcePath{ MC->CurrentProject()->resourceFolder(true) };

        if (relative)
        {
            fullResourcePath = GetPath(fullPath_);

            while (blocksetPath.StartsWith("./"))
            {
                blocksetPath = blocksetPath.Substring(2);
            }

            while (blocksetPath.StartsWith("../"))
            {
                blocksetPath = blocksetPath.Substring(3);
                fullResourcePath = AddTrailingSlash(fullResourcePath.Substring(0, RemoveTrailingSlash(fullResourcePath).FindLast('/')));
            }
        }
        else if (blocksetPath.StartsWith("/"))
        {
            fullResourcePath.Clear();
        }

        if (Blockset* blockSet{ EM->LoadBlockset(fullResourcePath + blocksetPath) })
        {
            XMLElement layerXML{ source.GetChild("gridlayer") };

            if (layerXML.IsNull())
                continue;

            while (!layerXML.IsNull())
            {
                unsigned layer{ layerXML.GetUInt("id")};
                XMLElement gridBlockXML{ layerXML.GetChild("gridblock") };

                while (!gridBlockXML.IsNull())
                {
                    if (gridBlockXML.GetInt("set") == blockSetXML.GetInt("id"))
                    {
                        const IntVector3 coords{ gridBlockXML.GetIntVector3("coords") };
                        SetGridBlock(coords,
                                     gridBlockXML.GetQuaternion("rot"),
                                     layer,
                                     blockSet->GetBlockById(gridBlockXML.GetUInt("block")));

                        if (gridBlockXML.HasAttribute("elevation"))
                        {
                            const String elevationString{ gridBlockXML.GetAttribute("elevation") };
                            const String normalsString{ gridBlockXML.GetAttribute("normals") };
                            const String spanString{ gridBlockXML.GetAttribute("span") };
                            SetProfile(coords, layer, HeightProfile::fromString(elevationString, normalsString, spanString));
                        }
                    }

                    gridBlockXML = gridBlockXML.GetNext("gridblock");
                }

                layerXML = layerXML.GetNext("gridlayer");
            }
        }

        blockSetXML = blockSetXML.GetNext("blockset");
    }

    return true;
}

bool BlockMap::Save()
{
    for (Blockset* bs: GetUsedBlocksets())
    {
        if (!bs->Save())
            return false;
    }

    if (!isModified())
        return true;

    if (!fullPath_.Contains('/'))
        fullPath_ = QD(GetSubsystem<MainWindow>()->getSaveFilename(GetTypeInfo(), DQ(name_)));
    if (fullPath_.IsEmpty())
        return false;

    name_ = GetFileNameAndExtension(fullPath_);

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    XMLFile* mapXML{ cache->GetResource<XMLFile>(savedFullPath_) };

    if (!mapXML)
        mapXML = new XMLFile{ context_ };

    XMLElement rootElem{ mapXML->CreateRoot("blockmap") };

    if (!SaveXML(rootElem))
        return false;

    if (mapXML->SaveFile(fullPath_))
    {
        undoStack_.setClean();

        if (!savedFullPath_.IsEmpty() && savedFullPath_ != fullPath_)
            FS->Delete(savedFullPath_);

        savedName_ = name_;
        savedFullPath_ = fullPath_;

        return true;
    }

    return false;
}

String BlockMap::SaveTemp(const String& tempPath, const PODVector<Blockset*>& skip) const
{
    Project* p{ MC->CurrentProject() };

    for (Blockset* bs: GetUsedBlocksets())
    {
        if (!bs->isModified() || skip.Contains(bs))
            continue;
        else if (!bs->SaveTemp(tempPath))
            return "";
    }

    XMLFile* mapXML{ new XMLFile{ context_ } };
    XMLElement rootElem{ mapXML->CreateRoot("blockmap") };

    QDir tempDir{ DQ(tempPath) };
    const String localBlockmapPath{ p->localizedPath(GetPath(fullPath_)) };
    if (!localBlockmapPath.IsEmpty())
        tempDir.mkpath(DQ(localBlockmapPath));

    const String saveFileName{ AddTrailingSlash(tempPath) + p->localizedPath(fullPath_) };

    if (SaveXML(rootElem) && mapXML->SaveFile(saveFileName))
        return saveFileName;
    else
        return "";
}

bool BlockMap::SaveXML(XMLElement& dest) const
{
    dest.SetIntVector3("map_size", GetMapSize());
    dest.SetVector3("block_size", GetBlockSize());

    XMLElement attributesElem{ dest.CreateChild("attributes") };
    saveAttributes(attributesElem);

    HashMap<unsigned, Blockset*> blockSetsById{};
    unsigned blocksetId{ 0 };

    for (Blockset* blockSet: GetUsedBlocksets())
    {
        XMLElement blockSetElem{ dest.CreateChild("blockset") };
        blockSetsById[blocksetId] = blockSet;
        blockSetElem.SetInt("id", blocksetId);
        ++blocksetId;

        Project* p{ MC->CurrentProject() };
        blockSetElem.SetAttribute("name", p->localizedPath(blockSet->savedFullPath_));
    }

    HashMap<unsigned, XMLElement> layerElements{};
    for (int l{ 1 }; l <= 5; ++l)
    {
        layerElements[l] = dest.CreateChild("gridlayer");
        layerElements[l].SetUInt("id", l);
    }

    for (GridBlock* gridBlock: GetOccupiedGridBlocks())
    {
        Block* block{ gridBlock->GetBlock() };
        int blocksetId{ GetBlocksetId(block, blockSetsById) };

        if (blocksetId >= 0)
        {
            const IntVector3 coords{ gridBlock->GetCoords() };
            XMLElement gridBlockElem{ layerElements[gridBlock->GetLayer()].CreateChild("gridblock") };

            gridBlockElem.SetUInt("set", blocksetId);
            gridBlockElem.SetUInt("block", block->id());
            gridBlockElem.SetIntVector3("coords", coords);
            gridBlockElem.SetQuaternion("rot", gridBlock->GetRotation());

            if (TileInstance* tile{ gridBlock->Cast<TileInstance>() })
            {
                const HeightProfile& profile{ tile->heightProfile() };
                gridBlockElem.SetString("elevation", profile.elevation().toString() );
                if (!profile.isFlat(true))
                    gridBlockElem.SetString("normals", profile.normals().toString() );
                if (profile.span() != HeightProfile::DEFAULTSPAN)
                    gridBlockElem.SetString("span", profile.span().ToString());
            }
        }
    }

    for (const XMLElement& layerElement: layerElements.Values())
    {
        if (!layerElement.HasChild("gridblock"))
            dest.RemoveChild(layerElement);
    }

    return true;
}

void BlockMap::saveAttributes(XMLElement& dest) const
{
    Project* project{ MC->CurrentProject() };

    for (const Pair<String, Variant>& attribute: attributes_)
    {
        if (project->isValidMapAttribute(attribute))
        {
            XMLElement attrElem{ dest.CreateChild("attribute") };
            attrElem.SetAttribute("name", attribute.first_);
            attrElem.SetAttribute("type", attribute.second_.GetTypeName());
            attrElem.SetAttribute("value", attribute.second_.ToString());
        }
    }
}

void BlockMap::loadAttributes(const XMLElement& source)
{
    XMLElement attrElem{ source.GetChild("attribute") };

    while (!attrElem.IsNull())
    {
        Pair<String, Variant> attribute{};
        attribute.first_ = attrElem.GetAttribute("name");
        attribute.second_ = Variant{ attrElem.GetAttribute("type"), attrElem.GetAttribute("value") };
        attributes_.Push(attribute);

        attrElem = attrElem.GetNext("attribute");
    }
}

int BlockMap::GetBlocksetId(Block* block, HashMap<unsigned, Blockset*>& blockSetsById) const
{
    for (unsigned id: blockSetsById.Keys())
    {
        if (blockSetsById[id]->blocks().Contains(block))
            return id;
    }

    return -1;
}

Vector<Blockset*> BlockMap::GetUsedBlocksets() const
{
    Vector<Blockset*> blocksets{};
    for (const auto kv: blocksetUseCount_)
        blocksets.Push(kv.first);
    return blocksets;
}

Vector<GridBlock*> BlockMap::GetOccupiedGridBlocks() const
{
    Vector<GridBlock*> gridBlocks{};

    for (GridMap& layer: gridLayers_.Values())
    {
        for (Sheet& sheet: layer.Values())
        {
            for (GridBlock* gridBlock: sheet.Values())
            {
                if (gridBlock->GetBlock() != nullptr)
                    gridBlocks.Push(gridBlock);
            }
        }
    }

    return gridBlocks;
}

BlockInstance* BlockMap::GetBlockInstance(const IntVector3& coords, unsigned layer) const
{
    GridMap* const gridMap{ gridLayers_[layer] };
    BlockInstance* blockInstance{ nullptr };
    Sheet sheet{};

    if (gridMap->TryGetValue(coords.y_, sheet))
    {
        GridBlock* gridBlock{ nullptr };

        if (sheet.TryGetValue({ coords.x_, coords.z_ }, gridBlock))
            blockInstance = static_cast<BlockInstance*>(gridBlock);
    }

    return blockInstance;
}

Vector<BlockInstance*> BlockMap::GetInstancesOfBlock(Block* block) const
{
    Vector<BlockInstance*> blockInstances{};

    for (GridBlock* gridBlock: GetOccupiedGridBlocks())
    {
        if (gridBlock->GetBlock() == block)
            blockInstances.Push(gridBlock);
    }

    return blockInstances;
}

unsigned BlockMap::blocksetID(Block* block) const
{
    if (!block)
        return M_MAX_UNSIGNED;

    Vector<Blockset*> usedSets{ GetUsedBlocksets() };
    for (Blockset* set: usedSets)
        if (set->blocks().Contains(block))
            return usedSets.IndexOf(set);

    return M_MAX_UNSIGNED;
}

void BlockMap::SetGridBlock(const IntVector3& coords, const Quaternion& rotation, unsigned layer, Block* block)
{
    if (!Contains(coords))
        return;

    GridBlock* gridBlock{ nullptr };
    Sheet& sheet{ gridLayers_[layer][coords.y_] };
    const IntVector2 sheetCoords{ coords.x_, coords.z_ };
    sheet.TryGetValue(sheetCoords, gridBlock);
    Block* oldBlock{ (gridBlock ? gridBlock->GetBlock() : nullptr) };

    if (block)
    {
        const BlockType type{ block->type() };
        if (gridBlock && gridBlock->type() != type)
        {
            sheet.Erase(sheetCoords);
            gridBlock->GetNode()->Remove();
            gridBlock = nullptr;
        }

        if (!gridBlock)
        {
            Node* gridNode{ node_->CreateChild("GridBlock") };
            if (type == BLOCK_BLOCK)
                gridBlock = gridNode->CreateComponent<GridBlock>();
            else if (type == BLOCK_TILE)
                gridBlock = gridNode->CreateComponent<TileInstance>();

            gridBlock->Init(coords, layer);
            sheet.Insert({ sheetCoords, gridBlock});
        }

        gridBlock->SetBlock(block, rotation);
    }
    else if (gridBlock)
    {
        sheet.Erase(sheetCoords);
        gridBlock->GetNode()->Remove();
    }

    updateCount(oldBlock, block);
}

void BlockMap::SetProfile(const IntVector3& coords, unsigned layer, const HeightProfile& profile)
{
    GridBlock* gridBlock{ nullptr };
    const Sheet& sheet{ gridLayers_[layer][coords.y_] };
    const IntVector2 sheetCoords{ coords.x_, coords.z_ };
    sheet.TryGetValue(sheetCoords, gridBlock);

    if (!gridBlock || gridBlock->type() != BLOCK_TILE)
        return;

    TileInstance* tile{ gridBlock->Cast<TileInstance>() };
    tile->setProfile(profile);
}

void BlockMap::SetBlockSize(const Vector3& size)
{
    blockSize_ = size;

    UpdateCorners();
}

void BlockMap::setAttributes(const MapAttributes& mapAttributes)
{
    if (mapAttributes == attributes_)
        return;

    StringVector changed{};
    for (const Pair<String, Variant>& rhs: mapAttributes)
    {
        for (const Pair<String, Variant>& lhs: attributes_)
        {
            if (lhs.first_ == rhs.first_ && lhs.second_ != rhs.second_)
                changed.Push(lhs.first_);
        }
    }

    attributes_ = mapAttributes;

    for (const String& name: changed)
        GetSubsystem<MapPanel>()->updateAttributeInfo(name);
}

void BlockMap::SetMapSize(const IntVector3& size)
{
    const IntVector3 oldSize{ mapSize_ };
    mapSize_ = size;

    UpdateCorners();

    if (mapSize_.x_ > oldSize.y_ ||
        mapSize_.y_ > oldSize.y_ ||
        mapSize_.z_ > oldSize.z_)
    {
        //Expand
        for (int l{ 1 }; l <= NUM_LAYERS; ++l)
        {
            GridMap gridMap{};

            if (gridLayers_.Contains(l))
                gridMap = gridLayers_[l];

            for (int y{ 0 }; y < GetMapHeight(); ++y)
            {
                Sheet sheet{};

                if (y < oldSize.y_)
                    sheet = gridMap[y];

                gridMap[y] = sheet;
            }

            gridLayers_[l] = gridMap;
        }
    }
}

void BlockMap::SetMapSize(int w, int h, int d)
{
    SetMapSize({ w, h, d });
}

Vector3 BlockMap::GetCenter() const
{
    return .5f * blockSize_ * mapSize_;
}

HashSet<IntVector3> BlockMap::GetAllCoords() const
{
    HashSet<IntVector3> coords{};

    for (int x{ 0 }; x < GetMapWidth();  ++x)
    for (int y{ 0 }; y < GetMapHeight(); ++y)
    for (int z{ 0 }; z < GetMapDepth();  ++z)
    {
        coords.Insert({ x, y, z });
    }

    return coords;
}

HashSet<IntVector3> BlockMap::GetTileCornersCoords() const
{
    HashSet<IntVector3> coords{};
    PODVector<TileInstance*> tiles{};
    scene_->GetComponents<TileInstance>(tiles, true);

    for (TileInstance* tile: tiles)
    {
        if (tile->GetLayer() == EM->activeLayer())
            coords.Insert(tile->cornerCoordinates());
    }

    return coords;
}

std::map<TileInstance*, HashSet<IntVector3>> BlockMap::GetTilesAndCorners() const
{
    std::map<TileInstance*, HashSet<IntVector3>> tilesAndCoords{};

    PODVector<TileInstance*> tiles{};
    scene_->GetComponents<TileInstance>(tiles, true);

    for (TileInstance* tile: tiles)
    {
        if (tile->GetLayer() == EM->activeLayer())
            tilesAndCoords.insert({ tile, tile->cornerCoordinates() });
    }

    return tilesAndCoords;
}

PODVector<TileInstance*> BlockMap::GetTileInstances(const IntVector3& coords, unsigned layer)
{
    if (!EM)
        return {};

    if (layer == M_MAX_UNSIGNED)
        layer = EM->activeLayer();

    PODVector<TileInstance*> allTiles{};
    scene_->GetComponents<TileInstance>(allTiles, true);
    PODVector<TileInstance*> tiles{};

    for (TileInstance* tile: allTiles)
    {
        if (tile->GetLayer() == layer && tile->isWithinBounds(coords))
            tiles.Push(tile);
    }

    return tiles;
}

bool BlockMap::Contains(const IntVector3& coords) const
{
    return coords.x_ >= 0 &&
           coords.y_ >= 0 &&
           coords.z_ >= 0 &&
           coords.x_ < mapSize_.x_ &&
           coords.y_ < mapSize_.y_ &&
           coords.z_ < mapSize_.z_;
}

bool BlockMap::isModified() const
{
    return savedName_.IsEmpty() || savedName_ != name_ || !undoStack_.isClean();
}

void BlockMap::updateCenters()
{
    centerBillboards_->UpdateBillboards();
}

QString BlockMap::displayName() const
{
    QString displayName{ DQ(name_) };

    if (isModified())
        displayName.append('*');

    return displayName;
}

void BlockMap::updateCount(Block* oldBlock, Block* newBlock)
{
    if (oldBlock == newBlock)
        return;

    if (oldBlock)
    {
        Blockset* blockset{ oldBlock->blockset() };
        unsigned& blocksetCount{ blocksetUseCount_[blockset] };

        if (--blocksetCount == 0u)
            blocksetUseCount_.erase(blockset);
    }

    if (newBlock)
    {
        Blockset* blockset{ newBlock->blockset() };

        if (!blocksetUseCount_.contains(blockset))
            blocksetUseCount_[blockset] = 1u;
        else
            ++blocksetUseCount_[blockset];
    }
}
