/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDir>
#include <QFileDialog>

#include "../mainwindow.h"
#include "../docks/blockspanel.h"
#include "../project.h"
#include "tile.h"

#include "blockset.h"

Blockset::Blockset(Context* context): QObject(), Serializable(context),
    name_{},
    savedName_{},
    fullPath_{},
    savedFullPath_{},
    blocksModified_{ false }
{
    SubscribeToEvent(E_BLOCKMODIFIED, DRY_HANDLER(Blockset, HandleBlockModified));
}

Block* Blockset::GetBlockById(unsigned id)
{
    for (Block* b: blocks_)
    {
        if (b->id() == id)
            return b;
    }

    return nullptr;
}

bool Blockset::isModified() const
{
    return blocksModified_ || name_ != savedName_ || savedFullPath_.IsEmpty();
}

bool Blockset::LoadXML(const XMLElement& source)
{
    XMLElement blockXML{ source.GetChild("block") };
    while (blockXML)
    {
        HashMap<unsigned, String> materials{};

        if (!blockXML.GetAttribute("material").IsEmpty())
        {
            materials[SINGLE] = blockXML.GetAttribute("material");
        }
        else
        {
            XMLElement materialXML{ blockXML.GetChild("material")};
            while (materialXML)
            {
                materials[materialXML.GetUInt("index")] = materialXML.GetAttribute("name");
                materialXML = materialXML.GetNext("material");
            }
        }

        AddBlock(blockXML.GetAttribute("name"),
                 blockXML.GetAttribute("model"),
                 materials,
                 blockXML.GetUInt("id"));

        blockXML = blockXML.GetNext("block");
    }

    XMLElement tileXML{ source.GetChild("tile") };
    while (tileXML)
    {
        AddTile(tileXML.GetAttribute("name"),
                tileXML.GetAttribute("material"),
                tileXML.GetUInt("id"));

        tileXML = tileXML.GetNext("tile");
    }

    return true;
}

bool Blockset::Save()
{
    if (!isModified())
        return true;

    if (fullPath_.IsEmpty())
        fullPath_ = QD(GetSubsystem<MainWindow>()->getSaveFilename(GetTypeInfo(), DQ(name_)));
    if (fullPath_.IsEmpty())
        return false;

    name_ = GetFileNameAndExtension(fullPath_);

    XMLFile* blockSetXML{ new XMLFile(context_) };
    XMLElement rootElem{ blockSetXML->CreateRoot("blockset") };

    for (Block* b: blocks_)
        b->SaveXML(rootElem);

    File file{ context_, fullPath_, FILE_WRITE };

    if (blockSetXML->Save(file))
    {
        if (!savedFullPath_.IsEmpty() && savedFullPath_ != fullPath_)
            FS->Delete(savedFullPath_);

        savedName_ = name_;
        savedFullPath_ = fullPath_;
        blocksModified_ = false;
    }
    else
    {
        return false;
    }

    file.Close();

    return true;
}

bool Blockset::SaveTemp(const String& destination) const
{
    XMLFile* blockSetXML{ new XMLFile(context_) };
    XMLElement rootElem{ blockSetXML->CreateRoot("blockset") };

    for (Block* b: blocks_)
        b->SaveXML(rootElem);

    Project* p{ MC->CurrentProject() };

    QDir destDir{ DQ(destination) };
    const String localBlocksetFullPath{ p->localizedPath(fullPath_) };
    const QString localBlocksetPath{ DQ(GetPath(localBlocksetFullPath)) };
    destDir.mkpath(localBlocksetPath);

    if (blockSetXML->SaveFile(AddTrailingSlash(destination) + localBlocksetFullPath))
        return true;
    else
        return false;
}

QString Blockset::DisplayName() const
{
    QString displayName{ DQ(name_) };

    if (isModified())
        displayName.append('*');

    return displayName;
}

void Blockset::AddBlock(const String& blockName, const String& modelName, const Materials& materials, unsigned id)
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    Model* model{ cache->GetResource<Model>(modelName) };
    bool modified{ isModified() };

    if (model)
    {
        Block* block{ new Block{ context_, this } };

        if (id == NOID)
        {
            id = 0u;
            while (GetBlockById(id))
                ++id;

            blocksModified_ = true;
        }

        block->setId(id);
        block->setName(blockName);
        block->setModel(model);
        block->setMaterials(materials);

        blocks_.Push(block);
    }
    else
    {
        //Block loading failed while loading the blockset
        if (id != NOID)
            blocksModified_ = true;
    }

    if (modified != isModified())
        MainWindow::mainWindow_->handleFileModifiedStateChange();
}

void Blockset::AddTile(const String& tileName, const String& materialName, unsigned id)
{
    bool modified{ isModified() };
    Tile* tile{ new Tile{ context_, this } };

    if (id == NOID)
    {
        id = 0u;
        while (GetBlockById(id))
            ++id;

        blocksModified_ = true;
    }

    tile->setId(id);
    tile->setName(tileName);
    tile->setMaterials({ { SINGLE, materialName } });

    blocks_.Push(tile);

    if (modified != isModified())
        MainWindow::mainWindow_->handleFileModifiedStateChange();
}

void Blockset::RemoveBlock(Block* block)
{
    if (!block || !blocks_.Contains(block))
        return;

    bool modified{ isModified() };

    blocks_.Remove(block);
    blocksModified_ = true;

    if (modified != isModified())
    {
        MainWindow::mainWindow_->handleFileModifiedStateChange();
        GetSubsystem<BlocksPanel>()->updateBlocksetList();
    }
}

void Blockset::HandleBlockModified(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (blocks_.Contains(static_cast<Block*>(GetEventSender())))
        blocksModified_ = true;
}
