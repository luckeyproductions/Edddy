/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BLOCK_H
#define BLOCK_H

#include "../luckey.h"

class Blockset;

DRY_EVENT(E_BLOCKMODIFIED, BlockModified){}


#define SINGLE M_MAX_UNSIGNED
#define MAT_NONE GetSubsystem<ResourceCache>()->GetTempResource<Material>("Materials/None.xml")
using Materials = HashMap<unsigned, String>;

enum BlockType{ BLOCK_BLOCK, BLOCK_TILE, BLOCK_TRACK };

class Block: public QObject, public Serializable
{
    friend class EditMaster;

    Q_OBJECT
    DRY_OBJECT(Block, Serializable);

public:
    static void RegisterAttributes(Context* context);

    Block(Context* context, Blockset* blockset = nullptr);

    void setId(unsigned id) { id_ = id;  }
    void setName(const String& name) { name_ = name; }
    virtual void setModel(Model* model);
    void setMaterial(unsigned index, Material* material);
    void setMaterial(Material* material);
    void setMaterials(const Materials& materials);

    unsigned id() const { return id_; }
    String name() const { return name_; }
    virtual BlockType type() const { return BLOCK_BLOCK; }

    Model* model() const;
    virtual String modelName() const;
    unsigned numGeometries() const;

    Material* material() const;
    SharedPtr<Material> material(unsigned index) const;
    const Materials& materials() const { return materials_; }

    Image* image();

    Blockset* blockset() const { return blockset_; }

    bool SaveXML(XMLElement& dest) const override;
    void updatePreview();

private:
    void createPreviewRenderer();
    void applyMaterials();

    Blockset* blockset_;
    unsigned id_;
    String name_;
    Materials materials_;

    Scene* previewScene_;
    Node* previewNode_;
    StaticModel* previewModel_;

    SharedPtr<Image> image_;
    SharedPtr<Texture2D> renderTexture_;
};

#endif // BLOCK_H
