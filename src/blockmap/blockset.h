/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef BLOCKSET_H
#define BLOCKSET_H

#include "../luckey.h"

#include "block.h"

#define NOID M_MAX_UNSIGNED

class Blockset: public QObject, public Serializable
{
    Q_OBJECT
    DRY_OBJECT(Blockset, Serializable);

public:
    Blockset(Context* context);

    bool isModified() const;
    bool LoadXML(const XMLElement& source) override;
    bool Save();
    bool SaveTemp(const String& destination) const;

    void setName(const QString& name)
    {
        name_ = QD(name);

        if (!fullPath_.IsEmpty())
            fullPath_ = fullPath_.Substring(0, fullPath_.FindLast('/') + 1).Append(name_);
    }

    const Vector<Block*>& blocks() const { return blocks_; }
    void AddBlock(const String& blockName, const String& modelName, const Materials& materials, unsigned id = NOID);
    void AddTile(const String& tileName, const String& materialName, unsigned id = NOID);
    void RemoveBlock(Block* block);

    Block* GetBlockById(unsigned id);
    QString DisplayName() const;

    Block** begin() { return blocks_.Begin().ptr_; }
    Block** end()   { return blocks_.End().ptr_; }

    String name_;
    String savedName_;
    String fullPath_;
    String savedFullPath_;
    bool blocksModified_;

private:
    void HandleBlockModified(StringHash eventType, VariantMap& eventData);

    Vector<Block*> blocks_;
};

#endif // BLOCKSET_H
