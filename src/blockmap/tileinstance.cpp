﻿/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "blockmap.h"
#include "../cyberplasm/cyberplasm.h"
#include "../editmaster.h"
#include "../tools/dig.h"

#include "tileinstance.h"

TileInstance::TileInstance(Context* context): GridBlock(context),
    profile_{}
{
    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(TileInstance, DrawNormals));
}

HashSet<IntVector3> TileInstance::cornerCoordinates() const
{
    HashSet<IntVector3> coords{};
    for (unsigned i{ 0u }; i < 4u; ++i)
        coords.Insert(cornerCoordinate(i));

    return coords;
}

IntVector3 TileInstance::cornerCoordinate(int index) const
{
    const int stepRotation{ RoundToInt(blockNode_->GetWorldRotation().YawAngle() / 90.f) };
    const IntVector3 cornerOffset{ profile_.cornerCoordinate(index) };
    const IntVector2 flatCornerOffset{ IntVector2{ cornerOffset.x_, cornerOffset.z_ }.Rotated(stepRotation) };
    const IntVector3 cornerCoordinate{ flatCornerOffset.x_, cornerOffset.y_, flatCornerOffset.y_ };
    return GetCoords() + cornerOffset;
}

bool TileInstance::isWithinBounds(const IntVector3& coords)
{
    const IntVector2 flatOriginCoords{ GetCoords().x_, GetCoords().z_ };
    const IntRect offsetBounds{ profile_.span().Min() + flatOriginCoords, profile_.span().Max() + flatOriginCoords };
    if (offsetBounds.IsInside({ coords.x_, coords.z_ }))
    {
        return coords.y_ >= GetCoords().y_ + profile_.min() &&
               coords.y_ <= GetCoords().y_ + profile_.max();
    }

    return false;
}

void TileInstance::update()
{
    if (!block_)
    {
        blockModel_->SetModel(nullptr);
        return;
    }

    generateTile();

    if (block_->material())
        blockModel_->SetMaterial(block_->material());
}

void TileInstance::generateTile()
{
    if (!blockMap_)
        return;

    Witch::Spell rect{ {}, Witch::Spell::QUAD };
    for (unsigned c{ 0u }; c < 4u; ++c)
        rect.Push(cornerRune(c, false));

    Cyberplasm* cyberplasm{ GetSubsystem<Cyberplasm>() };
    SharedPtr<Model> tileModel{ context_->CreateObject<Model>() };

    if (profile_.isFlat())
    {
        tileModel = cyberplasm->Summon({ rect }, 0);
    }
    else
    {
        const IntVector2 size{ profile_.size() };
        Vector3 boundsMin{  Vector3::ONE * M_INFINITY};
        Vector3 boundsMax{ -Vector3::ONE * M_INFINITY};

        const int lods{ 5 };
        tileModel->SetNumGeometries(1);
        tileModel->SetNumGeometryLodLevels(0, lods);

        for (int lod{ 0 }; lod < lods; ++lod)
        {
            const int r{ Max(1, 1 << (lods - lod - 1)) };
            const Witch::Form form{ rect.Cast({ r * size.x_ - 1, r * size.y_ - 1 }) };
            tileModel->SetGeometry(0, lod, cyberplasm->Conjure(form));
            tileModel->GetGeometry(0, lod)->SetLodDistance(lod * 23.5f);

            boundsMax = VectorMax(boundsMax, form.bounds_.min_);
            boundsMin = VectorMin(boundsMin, form.bounds_.max_);
        }

        tileModel->SetBoundingBox({ boundsMin, boundsMax });
    }

    blockModel_->SetModel(tileModel);
}

Witch::Rune TileInstance::cornerRune(int index, bool global) const
{
    const IntVector2 size{ profile_.size() };
    const Vector3 offCenter{ profile_.offCenter() * blockMap_->GetBlockSize() };
    const Vector3 halfSize{ blockMap_->GetBlockWidth() * size.x_ * .5f, blockMap_->GetBlockHeight() * .5f, blockMap_->GetBlockDepth() * size.y_ * .5f };
    const float blockHeight{ blockMap_->GetBlockHeight() };

    const int e{ profile_.elevation().At(index) };
    const IntVector3 offset{ HeightProfile::cornerOffset(index) * 2 - IntVector3{ 1, 0, 1 } };
    const Vector3 pos{ halfSize * offset + Vector3::UP * (e * blockHeight - halfSize.y_) + offCenter };
    const Vector3 normal{ profile_.normal(index) };

    Witch::Rune rune{ pos, normal };

    if (global)
    {
        return rune.Transformed(node_->GetWorldTransform());
    }
    else
    {
        const Quaternion rot{ blockNode_->GetRotation() };
        return rune.Transformed(Matrix3x4{ {}, rot.Inverse(), blockNode_->GetScale() });
    }
}

int TileInstance::indexFromCornerCoords(const IntVector3& coords)
{
    for (unsigned e{ 0u }; e < 4u; ++e)
    {
        const IntVector3 cornerCoords{ cornerCoordinate(e) };
        if (coords == cornerCoords)
            return e;
    }

    return -1;
}

void TileInstance::DrawNormals(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
    Tool* tool{ EM->GetTool() };

    if (!debug || !tool)
        return;

    Dig* digTool{ tool->Cast<Dig>() };
    if (!digTool)
        return;

    const float blockHeight{ blockMap_->GetBlockHeight() };

    for (int c{ 0 }; c < 4; ++c)
    {
        Witch::Rune rune{ cornerRune(c, true) };
        debug->AddLine(rune.Position() + rune.Normal() * .05f, rune.Position() + rune.Normal() * blockHeight * .25f, Color::ORANGE, true);
    }
}
