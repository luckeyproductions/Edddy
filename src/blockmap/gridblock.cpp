/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../inputmaster.h"
#include "../edddycam.h"
#include "../edddycursor.h"
#include "blockmap.h"

#include "gridblock.h"

GridBlock::GridBlock(Context* context): BlockInstance(context),
    coords_{ IntVector3::ONE * M_MAX_INT }
{
}

void GridBlock::Init(const IntVector3& coords, unsigned layer)
{
    SetCoords(coords);
    layer_ = layer;
}

void GridBlock::SetCoords(const IntVector3& coords)
{
    if (coords_ == coords)
        return;

    coords_ = coords;
    node_->SetPosition(blockMap_->coordsToPosition(coords_));
}

bool GridBlock::withinLock() const
{
    return withinLock(coords_);
}

bool GridBlock::withinLock(const IntVector3& coords)
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };
    const std::bitset<3> axisLock{ cursor->GetAxisLock() };
    const IntVector3 cursorCoords{ cursor->GetCoords() };

    for (int a{ 0 }; a < 3; ++a)
    {
        if (!axisLock.test(a) && coords.Data()[a] != cursorCoords.Data()[a])
            return false;
    }

    return true;
}
