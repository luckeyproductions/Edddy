/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "edddycam.h"
#include "edddycursor.h"
#include "inputmaster.h"
#include "blockmap/blockmap.h"
#include "editmaster.h"
#include "view3d.h"

EdddyCam::EdddyCam(Context *context): Camera(context)
{
}

void EdddyCam::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Camera::OnNodeSet(node);

    SetFarClip(4096.f);
    SetFov(60.f);
}

void EdddyCam::SetView(View3D* view)
{
    view3d_ = view;
}

void EdddyCam::resetPosition(BlockMap* blockMap)
{
    if (!blockMap)
        return;

    node_->SetRotation({ 23.f, 0.f, 0.f });
    node_->SetPosition(-Max(Max(blockMap->GetMapWidth(),
                                blockMap->GetMapHeight()),
                                blockMap->GetMapDepth()) * node_->GetDirection() * Sqrt(blockMap->GetBlockSize().Length() * .42f)
                       + blockMap->GetCenter() + Vector3::DOWN * blockMap->GetMapHeight() * .666f);
}

void EdddyCam::HandleMapChange(BlockMap* blockMap)
{
    blockMap->GetScene()->AddChild(node_);
}

void EdddyCam::ToggleOrthogaphic()
{
    SetOrthographic(!IsOrthographic());

    if (view3d_)
        view3d_->UpdateView();
}

void EdddyCam::Move(const Vector3& movement, MoveType type)
{
    if (movement == Vector3::ZERO)
        return;

    EdddyCursor* cursor{ EdddyCursor::cursor_ };
    const Vector3 cursorPosition{ cursor->GetNode()->GetPosition() };
    const float cursorDistance{ cursorPosition.DistanceToPoint(GetPosition()) };

    const bool ortho{ IsOrthographic() };
    const float panSpeed{ 1.5f + (ortho ? (GetOrthoSize() * 1.3f) : (cursorDistance * Pow(GetFov(), 2.f) * .00023f)) };
    const float rotationSpeed{ 235.f };

    switch (type)
    {
    case MT_FOV:
    {
        if (!ortho)
            SetFov(Clamp(GetFov() + 23.f * movement.y_, 5.f, 120.f));
        else
            node_->Translate(Vector3::BACK * movement.y_ * panSpeed);

    } break;
    case MT_MOUSEPAN:
    {
        Vector3 scaledMovement{ movement * panSpeed };

        if (!ortho)
        {
            node_->Translate(node_->GetRight()     * -scaledMovement.x_ +
                             node_->GetUp()        *  scaledMovement.y_ +
                             node_->GetDirection() *  scaledMovement.z_, TS_WORLD);
        }
        else
        {
            SetOrthoSize(Max(1.f, GetOrthoSize() + scaledMovement.z_ * -1.3f));
            const Vector3 lockVector{ cursor->GetLockVector() };

            if (!Equals(lockVector.LengthSquared(), 1.f)) ///Needs more cases or generalisation
            {
                if (lockVector.y_ == 0.f)
                    scaledMovement.y_ /= Abs(node_->GetDirection().DotProduct(Vector3::UP) * M_SQRT2);
//                else
//                    scaledMovement.x_ /= Abs(node_->GetDirection().DotProduct(Vector3::FORWARD) * M_SQRT2);
            }

            node_->Translate((lockVector * node_->GetRight()).Normalized() * -scaledMovement.x_
                           + (lockVector * node_->GetUp()).Normalized()    *  scaledMovement.y_, TS_WORLD);

        }
    } break;
    case MT_KEYPAN:
    {
        const Vector3 scaledMovement{ movement * panSpeed };

        if (!ortho)
        {
            const Quaternion camRot{ node_->GetRotation() };
            const float pitch{ camRot.PitchAngle() };
            const Vector3 forward{ Abs(pitch) == 90.f ? (node_->GetWorldUp() * Sign(pitch)).ProjectOntoPlane(Vector3::UP).Normalized()
                                                      : Quaternion{ camRot.YawAngle(), Vector3::UP } * Vector3::FORWARD };

            node_->Translate(node_->GetRight() * -scaledMovement.x_ +
                             Vector3::UP       *  scaledMovement.y_ +
                             forward           *  scaledMovement.z_, TS_WORLD);
        }
    } break;
    default: case MT_ROTATE:
    {
        float pitchDelta{ ClampedPitch(movement.y_ * rotationSpeed) };

        node_->RotateAround(cursorPosition,
                            Quaternion{ movement.x_ * rotationSpeed, Vector3::UP } *
                            Quaternion{ pitchDelta, node_->GetRight() }, TS_WORLD);
    }
    }

    if (view3d_)
        view3d_->UpdateView();
}

float EdddyCam::ClampedPitch(float pitchDelta)
{
    float resultingPitch{ node_->GetRotation().PitchAngle() + pitchDelta };

    if (resultingPitch > PITCH_MAX)
        pitchDelta -= resultingPitch - PITCH_MAX;
    else if (resultingPitch < PITCH_MIN)
        pitchDelta -= resultingPitch - PITCH_MIN;

    return pitchDelta;
}
