/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef EDDDYCURSOR_H
#define EDDDYCURSOR_H

#include "luckey.h"

#include "tools/tool.h"

DRY_EVENT(E_CURSORSTEP, CursorStep){}

class EdddyCursor: public LogicComponent
{
    DRY_OBJECT(EdddyCursor, LogicComponent);

public:
    static EdddyCursor* cursor_;
    EdddyCursor(Context* context);
    void OnNodeSet(Node* node) override;
    void Update(float timeStep) override;

    bool IsHidden() const { return hidden_; }
    void Hide();
    void Show();

    void UpdateSize();
    void SetAxisLock(std::bitset<3> lock);
    std::bitset<3> GetAxisLock() const { return axisLock_; }
    IntVector3 GetLockVector() const { return { 1 * axisLock_[0],
                                                1 * axisLock_[1],
                                                1 * axisLock_[2] }; }

    void Step(IntVector3 step);
    void SetCoords(const IntVector3& coords, const IntVector3& sub = IntVector3::ZERO);
    IntVector3 GetCoords() const { return coords_; }
    IntVector3 subCoords() const { return subCoords_; }
    Vector3 positionFromCoords() const;

    void MoveTo(const Vector3& position);

    void Rotate(bool clockWise);
    void SetRotation(const Quaternion& rot);
    Quaternion GetRotation() const { return rotation_; }

    void SetMouseRay(const Ray& ray) { mouseRay_ = ray; }
    void HandleMouseMove();

    void SetBlockMap(BlockMap* blockMap);
    BlockMap* GetBlockMap() const { return blockMap_; }
    Tool* tool() const;
    IntVector3 subStep() const
    {
        if (tool())
            return tool()->subStep();
        else
            return IntVector3::ONE;
    }

    void AddPreviewNode(const Vector3& position, const Quaternion& rotation = Quaternion::IDENTITY);
    void AddPreviewNode(const IntVector3& coords, const Quaternion& rotation);
    void RemoveInstanceNode(Node* node);
    void ClearPreview();
    PODVector<Node*> GetPreviewNodes() const { return previewNodes_; }

private:
    void AddCursorPreviewInstance();

    void HandleCurrentToolChanged(StringHash eventType, VariantMap& eventData);
    void UpdateModel(StringHash eventType, VariantMap& eventData);
    void DrawDebug(StringHash eventType, VariantMap& eventData);

    BlockMap* blockMap_;
    IntVector3 coords_;
    IntVector3 subCoords_;
    Ray mouseRay_;
    Quaternion rotation_;
    bool hidden_;

    Node* boxNode_;
    StaticModel* boxModel_;
    Node* blockNode_;
    Pair<StaticModelGroup*, StaticModelGroup*> blockModelGroups_;
    PODVector<Node*> previewNodes_;

    std::bitset<3> axisLock_;
};


#endif // EDDDYCURSOR_H
