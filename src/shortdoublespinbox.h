#ifndef SHORTDOUBLESPINBOX_H
#define SHORTDOUBLESPINBOX_H

#include <QKeyEvent>

#include <QDoubleSpinBox>

class ShortDoubleSpinBox: public QDoubleSpinBox
{
    Q_OBJECT

public:
    explicit ShortDoubleSpinBox(QWidget* parent = nullptr);

private:
    QString textFromValue(double value) const override;
    void keyPressEvent(QKeyEvent* e) override;

private slots:
    void roundResult();
};

#endif // SHORTDOUBLESPINBOX_H
