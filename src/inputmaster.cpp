/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "editmaster.h"
#include "castmaster.h"
#include "edddycam.h"
#include "edddycursor.h"

#include "tools/tool.h"

#include "inputmaster.h"

using namespace LucKey;

InputMaster::InputMaster(Context* context) : Object(context),
    cursor_{ nullptr },
    pressedJoystickButtons_{},
    shiftDown_{ false },
    ctrlDown_{ false },
    altDown_{ false }
{
    for (int a{ 0 }; a < ALL_ACTIONS; ++a)
        actionTime_[a] = 0.f;

    joystickButtonBindings_[CONTROLLER_BUTTON_DPAD_UP]       = ACTION_UP;
    joystickButtonBindings_[CONTROLLER_BUTTON_DPAD_DOWN]     = ACTION_DOWN;
    joystickButtonBindings_[CONTROLLER_BUTTON_DPAD_LEFT]     = ACTION_LEFT;
    joystickButtonBindings_[CONTROLLER_BUTTON_DPAD_RIGHT]    = ACTION_RIGHT;
    joystickButtonBindings_[CONTROLLER_BUTTON_RIGHTSHOULDER] = ACTION_FORWARD;
    joystickButtonBindings_[CONTROLLER_BUTTON_LEFTSHOULDER]  = ACTION_BACK;
//    joystickButtonBindings_[CONTROLLER_BUTTON_RIGHTTRIGGER] = ACTION_ROTATE_CW;
//    joystickButtonBindings_[CONTROLLER_BUTTON_LEFTTRIGGER]  = ACTION_ROTATE_CCW;
    joystickButtonBindings_[CONTROLLER_BUTTON_A]             = ACTION_CONFIRM;
    joystickButtonBindings_[CONTROLLER_BUTTON_B]             = ACTION_CANCEL;

    SubscribeToEvent(E_JOYSTICKBUTTONDOWN, DRY_HANDLER(InputMaster, HandleJoyButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONUP, DRY_HANDLER(InputMaster, HandleJoyButtonUp));
    SubscribeToEvent(E_JOYSTICKAXISMOVE, DRY_HANDLER(InputMaster, HandleJoystickAxisMove));
    SubscribeToEvent(E_CURSORSTEP, DRY_HANDLER(InputMaster, HandleCursorStep));

    INPUT->SetMouseVisible(true);
}

void InputMaster::HandleCursorStep(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    actionTime_[ACTION_CONFIRM] = ACTION_INTERVAL;

    Tool* tool{ EM->GetTool() };
    if (tool)
        tool->UpdatePreview();
}

void InputMaster::HandleJoyButtonDown(StringHash /*eventType*/, VariantMap& eventData)
{
    pressedJoystickButtons_.Insert( eventData[JoystickButtonDown::P_BUTTON].GetInt() );
}

void InputMaster::UpdateModifierKeys(Qt::KeyboardModifiers modifiers)
{
    const bool oldShift{ shiftDown_ };
    const bool oldCtrl { ctrlDown_  };
    const bool oldAlt  { altDown_   };

    shiftDown_ = (modifiers & Qt::ShiftModifier  ) != 0;
    ctrlDown_  = (modifiers & Qt::ControlModifier) != 0;
    altDown_   = (modifiers & Qt::AltModifier    ) != 0;

    if (shiftDown_ != oldShift
     || ctrlDown_  != oldCtrl
     || altDown_   != oldAlt)
    {
        SendEvent(E_CURSORSTEP);
    }
}

void InputMaster::HandleJoyButtonUp(StringHash /*eventType*/, VariantMap &eventData)
{
    pressedJoystickButtons_.Erase(eventData[JoystickButtonUp::P_BUTTON].GetInt());
}

void InputMaster::HandleJoystickAxisMove(StringHash /*eventType*/, VariantMap& eventData)
{
    int axis{ eventData[JoystickAxisMove::P_AXIS].GetInt() };
    float position{ eventData[JoystickAxisMove::P_POSITION].GetFloat() };

    if (axis == 0)
        leftStickPosition_.x_ = position;
    else if (axis == 1)
        leftStickPosition_.y_ = -position;
    else if (axis == 2)
        rightStickPosition_.x_ = position;
    else if (axis == 3)
        rightStickPosition_.y_ = -position;
}
