/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef VIEW3D_H
#define VIEW3D_H

#include "luckey.h"

#include <QWidget>

class QComboBox;

class View3D : public QWidget, public Object
{
    Q_OBJECT
    DRY_OBJECT(View3D, Object)

public:
    explicit View3D(Context* context);
    ~View3D();

    static View3D* Active() { return active_; }
    static bool simulatedMiddleMouse_;
    static bool flyMode_;
    static Vector<View3D*> views_;
    static void updateMapBoxes()
    {
        for (View3D* view: View3D::views_)
            view->UpdateMapBox();
    }

    void SetMap(BlockMap* blockMap);
    BlockMap* GetBlockMap() const { return blockMap_; }

    Node* cameraNode() const { return cameraNode_; }
    void CreateCamera(StringHash, VariantMap&);
    void CreateCamera();
    void resetCameraPosition();
    void setCameraTransform(const Matrix3x4& transform);

    void UpdateView();
    void UpdateView(StringHash, VariantMap&);
    void CreateRenderTexture();
    void UpdateMapBox();
    bool IsActive() const { return this == active_; }

    void Activate();
    void Split();
    void EnableFlyMode();

public slots:
    void SetMap(int index);

protected:
    void resizeEvent(QResizeEvent* event) override;
    void paintEvent(QPaintEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;
    void keyReleaseEvent(QKeyEvent* event) override;

private:
    static QPixmap ImageToPixmap(Image* image)
    {
        QImage qImage{ image->GetData(),
                    image->GetWidth(),
                    image->GetHeight(),
                    QImage::Format_RGBA8888};

        return QPixmap::fromImage(qImage);
    }

    void UpdateViewport();
    void PaintBorder();
    void UpdatePixmap();
    void HandleCursorStep(const StringHash eventType, VariantMap& eventData);
    void Pan();

    void PaintView();
    void PaintView(const StringHash eventType, VariantMap& eventData);

    static View3D* active_;
    static IntVector3 keyPan_;

    Vector3 toPan_;
    int wheelAccumulator_;

    QComboBox* mapBox_;
    QPoint previousMousePos_;

    SharedPtr<Texture2D> renderTexture_;
    SharedPtr<Image> image_;
    QPixmap pixmap_;
    SharedPtr<Node> cameraNode_;
    EdddyCam* edddyCam_;
    BlockMap* blockMap_;

    std::map<BlockMap*, Matrix3x4> cameraTransforms_;
};

#endif // VIEW3D_H
